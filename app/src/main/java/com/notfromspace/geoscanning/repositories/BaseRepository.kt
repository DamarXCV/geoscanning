package com.notfromspace.geoscanning.repositories

import android.util.Log
import com.notfromspace.geoscanning.extensions.PersistentData
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

open class BaseRepository {
    companion object {
        internal const val BASE_URL = "http://192.168.0.144/"
    }

    fun getClientWithAuth(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .addInterceptor { chain ->
                val auth = PersistentData.GetUserSession()?.token ?: ""
                Log.v("auth", auth)
                val request: Request =
                    chain.request().newBuilder().addHeader("Authorization", "Bearer $auth")
                        .build()
                chain.proceed(request)
            }
            .build()
    }

    fun getClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .build()
    }

    fun getService(): Retrofit.Builder {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
    }
}