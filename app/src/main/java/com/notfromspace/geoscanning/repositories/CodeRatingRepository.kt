package com.notfromspace.geoscanning.repositories

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.notfromspace.geoscanning.apis.CodeRatingService
import com.notfromspace.geoscanning.models.CodeRatingRequest
import com.notfromspace.geoscanning.models.CreateCodeRating
import com.notfromspace.geoscanning.models.ScanCode
import com.notfromspace.geoscanning.models.ScanCodeRequest
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CodeRatingRepository : BaseRepository() {
    private val codeRatingService: CodeRatingService
    private val scanCodeLiveData: MutableLiveData<ScanCode> = MutableLiveData<ScanCode>()
    private var networkErrorLiveData: MutableLiveData<String> = MutableLiveData<String>("")

    fun logCode(
        key: String
    ) {
        codeRatingService.LogCode(key)
            .enqueue(object : Callback<ScanCodeRequest> {
                override fun onResponse(
                    call: Call<ScanCodeRequest>,
                    response: Response<ScanCodeRequest>
                ) {
                    Log.v("onResponse", response.toString())
                    if (response.isSuccessful) {
                        scanCodeLiveData.postValue(response.body()?.codeRatingRequestToCodeRating())
                    } else {

                    }
                }

                override fun onFailure(
                    call: Call<ScanCodeRequest>,
                    t: Throwable
                ) {
                    Log.v("onFailure", t.toString())
                    networkErrorLiveData.postValue("Internal Error")
                }


            })
    }


    fun rateCode(data: CreateCodeRating) {
        data.codeId = scanCodeLiveData.value?.codeId
        if (data.codeId == null) return

        codeRatingService.RateCode(data.codeId!!, data.dif, data.`fun`, data.comment)
            .enqueue(object : Callback<CodeRatingRequest> {
                override fun onResponse(
                    call: Call<CodeRatingRequest>,
                    response: Response<CodeRatingRequest>
                ) {
                    Log.v("onResponse", response.toString())
                    if (response.isSuccessful) {

                    } else {
                    }
                }

                override fun onFailure(
                    call: Call<CodeRatingRequest>,
                    t: Throwable
                ) {
                    Log.v("onFailure", t.toString())
                    networkErrorLiveData.postValue("Internal Error")
                }


            })
    }

    fun getNetworkErrorLiveData(): LiveData<String> {
        return networkErrorLiveData
    }

    init {
        val client = getClientWithAuth()

        codeRatingService = getService()
            .client(client)
            .build()
            .create(CodeRatingService::class.java)
    }
}