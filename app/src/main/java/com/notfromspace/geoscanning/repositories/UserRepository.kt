package com.notfromspace.geoscanning.repositories

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.notfromspace.geoscanning.apis.UserService
import com.notfromspace.geoscanning.models.User
import com.notfromspace.geoscanning.models.UserRequest
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserRepository : BaseRepository() {
    private val userService: UserService
    private val userLiveData: MutableLiveData<User> = MutableLiveData<User>(null)
    private var networkErrorLiveData: MutableLiveData<String> = MutableLiveData<String>("")


    fun getUser(
        userId: Int
    ) {
        userService.GetUser(userId)
            .enqueue(object : Callback<UserRequest> {
                override fun onResponse(
                    call: Call<UserRequest>,
                    response: Response<UserRequest>
                ) {
                    Log.v("onResponse", response.toString())
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            val code = response.body()!!.userRequestToUser()
                            userLiveData.postValue(code)
                            networkErrorLiveData.postValue("")
                        }
                    } else {
                        networkErrorLiveData.postValue(response.message())
                    }
                }

                override fun onFailure(
                    call: Call<UserRequest>,
                    t: Throwable
                ) {
                    Log.v("onFailure", t.toString())

                    networkErrorLiveData.postValue("Internal Error")
                }


            })
    }

    fun getUserLiveData(): LiveData<User> {
        return userLiveData
    }

    fun getNetworkErrorLiveData(): LiveData<String> {
        return networkErrorLiveData
    }

    init {
        val client = getClientWithAuth()

        userService = getService()
            .client(client)
            .build()
            .create(UserService::class.java)
    }
}