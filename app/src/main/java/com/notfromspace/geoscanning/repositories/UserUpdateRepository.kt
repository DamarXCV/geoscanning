package com.notfromspace.geoscanning.repositories

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.notfromspace.geoscanning.apis.UserService
import com.notfromspace.geoscanning.models.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserUpdateRepository : BaseRepository() {
    private val userService: UserService
    private val userLiveData: MutableLiveData<User> = MutableLiveData<User>(null)
    private val userOldLiveData: MutableLiveData<UserSettings> = MutableLiveData<UserSettings>(null)
    private var networkErrorLiveData: MutableLiveData<String> = MutableLiveData<String>("")


    fun updateUser(
        data: UserUpdate
    ) {
        userService.UpdateUser(
            data.firstname,
            data.lastname,
            data.alias,
            data.hometown,
            data.country,
            data.email,
            data.description,
            data.profileImage
        )
            .enqueue(object : Callback<UserRequest> {
                override fun onResponse(
                    call: Call<UserRequest>,
                    response: Response<UserRequest>
                ) {
                    Log.v("onResponse", response.toString())
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            val responseData = response.body()!!.userRequestToUser()
                            userLiveData.postValue(responseData)
                            networkErrorLiveData.postValue("")
                        }
                    } else {
                        networkErrorLiveData.postValue(response.message())
                    }
                }

                override fun onFailure(
                    call: Call<UserRequest>,
                    t: Throwable
                ) {
                    Log.v("onFailure", t.toString())

                    networkErrorLiveData.postValue("Internal Error")
                }


            })
    }

    fun getUser(
        userId: Int
    ) {
        userService.GetUserSettings()
            .enqueue(object : Callback<UserSettingsRequest> {
                override fun onResponse(
                    call: Call<UserSettingsRequest>,
                    response: Response<UserSettingsRequest>
                ) {
                    Log.v("onResponse", response.toString())
                    if (response.isSuccessful) {
                        if (response.body() != null) {

                            val code = response.body()!!.userSettingsRequestToUserSettings()
                            userOldLiveData.postValue(code)
                            networkErrorLiveData.postValue("")
                        }
                    } else {
                        networkErrorLiveData.postValue(response.message())
                    }
                }

                override fun onFailure(
                    call: Call<UserSettingsRequest>,
                    t: Throwable
                ) {
                    Log.v("onFailure", t.toString())

                    networkErrorLiveData.postValue("Internal Error")
                }


            })
    }

    fun getUserLiveData(): LiveData<User> {
        return userLiveData
    }

    fun getUserOldLiveData(): LiveData<UserSettings> {
        return userOldLiveData
    }

    fun getNetworkErrorLiveData(): LiveData<String> {
        return networkErrorLiveData
    }

    init {
        val client = getClientWithAuth()

        userService = getService()
            .client(client)
            .build()
            .create(UserService::class.java)
    }
}