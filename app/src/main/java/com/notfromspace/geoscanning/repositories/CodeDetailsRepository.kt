package com.notfromspace.geoscanning.repositories

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.JsonObject
import com.notfromspace.geoscanning.apis.CodeDetailsService
import com.notfromspace.geoscanning.models.CodeDetails
import com.notfromspace.geoscanning.models.CodeDetailsRequest
import com.notfromspace.geoscanning.models.QRCodeRequest
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CodeDetailsRepository : BaseRepository() {
    private val codeDetailsService: CodeDetailsService
    private val codeDetailsLiveData: MutableLiveData<CodeDetails> =
        MutableLiveData<CodeDetails>(null)
    private var codeDateStringLiveData: MutableLiveData<String> = MutableLiveData<String>("")
    private var qrCodeLiveData: MutableLiveData<String> = MutableLiveData<String>("")
    private var networkErrorLiveData: MutableLiveData<String> = MutableLiveData<String>("")

    fun getCode(codeId: String) {
        codeDetailsService.GetCode(codeId)
            .enqueue(object : Callback<CodeDetailsRequest> {
                override fun onResponse(
                    call: Call<CodeDetailsRequest>,
                    response: Response<CodeDetailsRequest>
                ) {
                    Log.v("onResponse", response.toString())
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            val code = response.body()!!.CodeDetailsRequestToCodeDetails()
                            codeDetailsLiveData.postValue(code)
                            codeDateStringLiveData.postValue("${code.dateHidden.dayOfMonth}.${code.dateHidden.monthValue}.${code.dateHidden.year}")
                            networkErrorLiveData.postValue("")
                        }
                    } else {
                        codeDateStringLiveData.postValue("")
                        networkErrorLiveData.postValue(response.message())
                    }
                }

                override fun onFailure(
                    call: Call<CodeDetailsRequest>,
                    t: Throwable
                ) {
                    Log.v("onFailure", t.toString())

                    codeDateStringLiveData.postValue("")
                    networkErrorLiveData.postValue("Internal Error")
                }
            })
    }

    fun addBookmark() {
        val codeId = codeDetailsLiveData.value?.id ?: return

        codeDetailsService.AddBookmark(codeId)
            .enqueue(object : Callback<JsonObject> {
                override fun onResponse(
                    call: Call<JsonObject>,
                    response: Response<JsonObject>
                ) {
                    Log.v("onResponse", response.toString())
                    if (response.isSuccessful) {
                        val code = codeDetailsLiveData.value
                        if (code != null) {
                            code.isBookmarked = true
                            codeDetailsLiveData.postValue(code!!)
                        }
                    } else {
                        networkErrorLiveData.postValue("Internal Error")
                    }
                }

                override fun onFailure(
                    call: Call<JsonObject>,
                    t: Throwable
                ) {
                    Log.v("onFailure", t.toString())
                    networkErrorLiveData.postValue("Internal Error")
                }
            })
    }

    fun removeBookmark() {
        val codeId = codeDetailsLiveData.value?.id ?: return

        codeDetailsService.RemoveBookmark(codeId)
            .enqueue(object : Callback<JsonObject> {
                override fun onResponse(
                    call: Call<JsonObject>,
                    response: Response<JsonObject>
                ) {
                    Log.v("onResponse", response.toString())
                    if (response.isSuccessful) {
                        val code = codeDetailsLiveData.value
                        if (code != null) {
                            code.isBookmarked = false
                            codeDetailsLiveData.postValue(code!!)
                        }
                    } else {
                        networkErrorLiveData.postValue("Internal Error")
                    }
                }

                override fun onFailure(
                    call: Call<JsonObject>,
                    t: Throwable
                ) {
                    Log.v("onFailure", t.toString())
                    networkErrorLiveData.postValue("Internal Error")
                }
            })
    }

    fun getQRCode() {
        val codeId = codeDetailsLiveData.value?.id ?: return

        codeDetailsService.GetQRCode(codeId)
            .enqueue(object : Callback<QRCodeRequest> {
                override fun onResponse(
                    call: Call<QRCodeRequest>,
                    response: Response<QRCodeRequest>
                ) {
                    Log.v("onResponse", response.toString())
                    if (response.isSuccessful) {
                        qrCodeLiveData.postValue(response.body()?.data)
                    } else {
                        networkErrorLiveData.postValue("Internal Error")
                    }
                }

                override fun onFailure(
                    call: Call<QRCodeRequest>,
                    t: Throwable
                ) {
                    Log.v("onFailure", t.toString())
                    networkErrorLiveData.postValue("Internal Error")
                }
            })
    }

    fun resetNetworkErrorLiveData() {
        networkErrorLiveData.postValue("")
    }

    fun getCodeLiveData(): LiveData<CodeDetails> {
        return codeDetailsLiveData
    }

    fun getCodeDateStringLiveData(): LiveData<String> {
        return codeDateStringLiveData
    }

    fun getQRCodeLiveData(): LiveData<String> {
        return qrCodeLiveData
    }

    fun getNetworkErrorLiveData(): LiveData<String> {
        return networkErrorLiveData
    }

    init {
        val client = getClientWithAuth()

        codeDetailsService = getService()
            .client(client)
            .build()
            .create(CodeDetailsService::class.java)
    }
}