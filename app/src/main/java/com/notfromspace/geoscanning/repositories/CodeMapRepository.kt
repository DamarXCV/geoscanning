package com.notfromspace.geoscanning.repositories

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.notfromspace.geoscanning.apis.CodeMapService
import com.notfromspace.geoscanning.models.CodeMapItem
import com.notfromspace.geoscanning.models.CodeMapItemRequest
import com.yogeshpaliyal.universal_adapter.utils.Resource
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CodeMapRepository : BaseRepository() {
    private val codeMapService: CodeMapService
    private val codeMapLiveData: MutableLiveData<Resource<ArrayList<CodeMapItem>>> =
        MutableLiveData<Resource<ArrayList<CodeMapItem>>>(Resource.loading())
    private var networkErrorLiveData: MutableLiveData<String> = MutableLiveData<String>("")


    fun getMapList(
        long: Double,
        lat: Double,
        width: Double,
        height: Double
    ) {
        codeMapLiveData.postValue(Resource.loading(codeMapLiveData.value?.data))

        codeMapService.GetMapList(long, lat, width, height)
            .enqueue(object : Callback<List<CodeMapItemRequest>> {
                override fun onResponse(
                    call: Call<List<CodeMapItemRequest>>,
                    response: Response<List<CodeMapItemRequest>>
                ) {
                    Log.v("onResponse", response.toString())
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            val list: MutableList<CodeMapItem> = mutableListOf()
                            response.body()!!.forEach { item ->
                                list.add(item.codeMapItemRequestToCodeMapItem())
                            }

                            val tempArr = codeMapLiveData.value?.data ?: ArrayList()
                            tempArr.addAll(list)
                            codeMapLiveData.postValue(Resource.success(tempArr))

                            networkErrorLiveData.postValue("")
                        }
                    } else {
                        networkErrorLiveData.postValue(response.message())
                    }
                }

                override fun onFailure(
                    call: Call<List<CodeMapItemRequest>>,
                    t: Throwable
                ) {
                    Log.v("onFailure", t.toString())

                    networkErrorLiveData.postValue("Internal Error")
                }
            })
    }

    fun getCodesMapLiveData(): LiveData<Resource<ArrayList<CodeMapItem>>> {
        return codeMapLiveData
    }

    fun getNetworkErrorLiveData(): LiveData<String> {
        return networkErrorLiveData
    }

    init {
        val client = getClientWithAuth()

        codeMapService = getService()
            .client(client)
            .build()
            .create(CodeMapService::class.java)
    }
}