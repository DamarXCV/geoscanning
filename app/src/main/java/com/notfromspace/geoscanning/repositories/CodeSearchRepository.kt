package com.notfromspace.geoscanning.repositories

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.notfromspace.geoscanning.apis.CodeSearchService
import com.notfromspace.geoscanning.models.CodeListItem
import com.notfromspace.geoscanning.models.CodeListItemRequest
import com.yogeshpaliyal.universal_adapter.utils.Resource
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CodeSearchRepository : BaseRepository() {
    private val codeSearchService: CodeSearchService
    private val codeSearchLiveData: MutableLiveData<Resource<ArrayList<CodeListItem>>> =
        MutableLiveData<Resource<ArrayList<CodeListItem>>>(Resource.loading())
    private var networkErrorLiveData: MutableLiveData<String> = MutableLiveData<String>("")

    fun getSearchList(
        term: String,
        numberOfItems: Int,
        long: Float,
        lat: Float
    ) {
        codeSearchLiveData.postValue(Resource.loading(codeSearchLiveData.value?.data))

        codeSearchService.GetSearchList(term, numberOfItems, long, lat)
            .enqueue(object : Callback<List<CodeListItemRequest>> {
                override fun onResponse(
                    call: Call<List<CodeListItemRequest>>,
                    response: Response<List<CodeListItemRequest>>
                ) {
                    Log.v("onResponse", response.toString())
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            val list: MutableList<CodeListItem> = mutableListOf()
                            response.body()!!.forEach { item ->
                                list.add(item.codeListItemRequestToCodeListItem())
                            }

                            val tempArr = codeSearchLiveData.value?.data ?: ArrayList()
                            tempArr.addAll(list)
                            codeSearchLiveData.value = Resource.success(tempArr)
                            networkErrorLiveData.postValue("")
                        }
                    } else {
                        networkErrorLiveData.postValue(response.message())
                    }
                }

                override fun onFailure(
                    call: Call<List<CodeListItemRequest>>,
                    t: Throwable
                ) {
                    Log.v("onFailure", t.toString())

                    networkErrorLiveData.postValue("Internal Error")
                }
            })
    }

    fun getCodesListLiveData(): LiveData<Resource<ArrayList<CodeListItem>>> {
        return codeSearchLiveData
    }

    fun clearListLiveData() {
        codeSearchLiveData.value = Resource.success(arrayListOf())
    }

    fun getNetworkErrorLiveData(): LiveData<String> {
        return networkErrorLiveData
    }

    init {
        val client = getClientWithAuth()

        codeSearchService = getService()
            .client(client)
            .build()
            .create(CodeSearchService::class.java)
    }
}