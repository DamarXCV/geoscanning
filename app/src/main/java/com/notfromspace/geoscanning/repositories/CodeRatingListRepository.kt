package com.notfromspace.geoscanning.repositories

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.notfromspace.geoscanning.apis.CodeRatingService
import com.notfromspace.geoscanning.models.CodeRating
import com.notfromspace.geoscanning.models.CodeRatingRequest
import com.yogeshpaliyal.universal_adapter.utils.Resource
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.time.LocalDateTime

class CodeRatingListRepository : BaseRepository() {
    private val codeRatingService: CodeRatingService
    private val codeRatingListLiveData: MutableLiveData<Resource<ArrayList<CodeRating>>> =
        MutableLiveData<Resource<ArrayList<CodeRating>>>(Resource.loading())
    private var networkErrorLiveData: MutableLiveData<String> = MutableLiveData<String>("")

    fun getCodeRatingList(
        codeId: String,
        beforeDate: LocalDateTime,
        startPosition: Int,
        numberOfItems: Int
    ) {
        codeRatingListLiveData.postValue(Resource.loading(codeRatingListLiveData.value?.data))

        val date =
            "${beforeDate.year}.${beforeDate.monthValue}.${beforeDate.dayOfMonth} ${beforeDate.hour}:${beforeDate.minute}:${beforeDate.second}"

        codeRatingService.GetRatingList(codeId, date, startPosition, numberOfItems)
            .enqueue(object : Callback<List<CodeRatingRequest>> {
                override fun onResponse(
                    call: Call<List<CodeRatingRequest>>,
                    response: Response<List<CodeRatingRequest>>
                ) {
                    Log.v("onResponse", response.toString())
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            val list: MutableList<CodeRating> = mutableListOf()
                            response.body()!!.forEach { item ->
                                list.add(item.codeRatingRequestToCodeRating())
                            }

                            val tempArr = codeRatingListLiveData.value?.data ?: ArrayList()
                            tempArr.addAll(list)
                            codeRatingListLiveData.postValue(Resource.success(tempArr))

                            networkErrorLiveData.postValue("")
                        }
                    } else {
                        codeRatingListLiveData.postValue(Resource.error(response.message()))
                        networkErrorLiveData.postValue(response.message())
                    }
                }

                override fun onFailure(
                    call: Call<List<CodeRatingRequest>>,
                    t: Throwable
                ) {
                    Log.v("onFailure", t.toString())

                    codeRatingListLiveData.postValue(Resource.error("Internal Error"))
                    networkErrorLiveData.postValue("Internal Error")
                }
            })
    }

    fun getCodeRatingListLiveData(): LiveData<Resource<ArrayList<CodeRating>>> {
        return codeRatingListLiveData
    }

    fun getNetworkErrorLiveData(): LiveData<String> {
        return networkErrorLiveData
    }

    init {
        val client = getClientWithAuth()

        codeRatingService = getService()
            .client(client)
            .build()
            .create(CodeRatingService::class.java)
    }
}