package com.notfromspace.geoscanning.repositories

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.notfromspace.geoscanning.apis.AuthService
import com.notfromspace.geoscanning.extensions.PersistentData
import com.notfromspace.geoscanning.models.UserCreate
import com.notfromspace.geoscanning.models.UserSession
import com.notfromspace.geoscanning.models.UserSessionRequest
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserCreateRepository : BaseRepository() {
    private val authService: AuthService
    private val userLiveData: MutableLiveData<UserSession> = MutableLiveData<UserSession>(null)
    private var networkErrorLiveData: MutableLiveData<String> = MutableLiveData<String>("")


    fun createUser(
        data: UserCreate
    ) {
        authService.CreateUser(
            data.firstname,
            data.lastname,
            data.alias,
            data.hometown,
            data.country,
            data.email,
            data.description,
            data.profileImage,
            data.password
        )
            .enqueue(object : Callback<UserSessionRequest> {
                override fun onResponse(
                    call: Call<UserSessionRequest>,
                    response: Response<UserSessionRequest>
                ) {
                    Log.v("onResponse", response.toString())
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            val responseData = response.body()!!.userSessionRequestToUserSession()
                            PersistentData.SaveUserData(responseData)
                            userLiveData.postValue(responseData)
                            networkErrorLiveData.postValue("")
                        }
                    } else {
                        networkErrorLiveData.postValue(response.message())
                    }
                }

                override fun onFailure(
                    call: Call<UserSessionRequest>,
                    t: Throwable
                ) {
                    Log.v("onFailure", t.toString())

                    networkErrorLiveData.postValue("Internal Error")
                }


            })
    }

    fun getUserLiveData(): LiveData<UserSession> {
        return userLiveData
    }

    fun getNetworkErrorLiveData(): LiveData<String> {
        return networkErrorLiveData
    }

    init {
        val client = getClientWithAuth()

        authService = getService()
            .client(client)
            .build()
            .create(AuthService::class.java)
    }
}