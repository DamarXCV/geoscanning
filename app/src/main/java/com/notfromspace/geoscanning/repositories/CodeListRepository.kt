package com.notfromspace.geoscanning.repositories

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.notfromspace.geoscanning.apis.CodeListService
import com.notfromspace.geoscanning.models.CodeListItem
import com.notfromspace.geoscanning.models.CodeListItemRequest
import com.yogeshpaliyal.universal_adapter.utils.Resource
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CodeListRepository : BaseRepository() {
    private val codeListService: CodeListService
    private val codeListLiveData: MutableLiveData<Resource<ArrayList<CodeListItem>>> =
        MutableLiveData<Resource<ArrayList<CodeListItem>>>(Resource.loading())
    private var networkErrorLiveData: MutableLiveData<String> = MutableLiveData<String>("")


    fun getCreatedList(
        userId: Int,
        beforeDate: String,
        startPosition: Int,
        numberOfItems: Int
    ) {
        codeListLiveData.postValue(Resource.loading(codeListLiveData.value?.data))

        codeListService.GetCreatedList(userId, beforeDate, startPosition, numberOfItems)
            .enqueue(object : Callback<List<CodeListItemRequest>> {
                override fun onResponse(
                    call: Call<List<CodeListItemRequest>>,
                    response: Response<List<CodeListItemRequest>>
                ) {
                    Log.v("onResponse", response.toString())
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            val list: MutableList<CodeListItem> = mutableListOf()
                            response.body()!!.forEach { item ->
                                list.add(item.codeListItemRequestToCodeListItem())
                            }

                            val tempArr = codeListLiveData.value?.data ?: ArrayList()
                            tempArr.addAll(list)
                            codeListLiveData.postValue(Resource.success(tempArr))

                            networkErrorLiveData.postValue("")
                        }
                    } else {
                        networkErrorLiveData.postValue(response.message())
                    }
                }

                override fun onFailure(
                    call: Call<List<CodeListItemRequest>>,
                    t: Throwable
                ) {
                    Log.v("onFailure", t.toString())

                    networkErrorLiveData.postValue("Internal Error")
                }
            })
    }

    fun getFoundList(
        userId: Int,
        beforeDate: String,
        startPosition: Int,
        numberOfItems: Int
    ) {
        codeListService.GetFoundList(userId, beforeDate, startPosition, numberOfItems)
            .enqueue(object : Callback<List<CodeListItemRequest>> {
                override fun onResponse(
                    call: Call<List<CodeListItemRequest>>,
                    response: Response<List<CodeListItemRequest>>
                ) {
                    Log.v("onResponse", response.toString())
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            val list: MutableList<CodeListItem> = mutableListOf()
                            response.body()!!.forEach { item ->
                                list.add(item.codeListItemRequestToCodeListItem())
                            }

                            val tempArr = codeListLiveData.value?.data ?: ArrayList()
                            tempArr.addAll(list)
                            codeListLiveData.postValue(Resource.success(tempArr))

                            networkErrorLiveData.postValue("")
                        }
                    } else {
                        networkErrorLiveData.postValue(response.message())
                    }
                }

                override fun onFailure(
                    call: Call<List<CodeListItemRequest>>,
                    t: Throwable
                ) {
                    Log.v("onFailure", t.toString())

                    networkErrorLiveData.postValue("Internal Error")
                }
            })
    }

    fun getBookmarkList(
        userId: Int,
        beforeDate: String,
        startPosition: Int,
        numberOfItems: Int
    ) {
        codeListService.GetBookmarkList(userId, beforeDate, startPosition, numberOfItems)
            .enqueue(object : Callback<List<CodeListItemRequest>> {
                override fun onResponse(
                    call: Call<List<CodeListItemRequest>>,
                    response: Response<List<CodeListItemRequest>>
                ) {
                    Log.v("onResponse", response.toString())
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            val list: MutableList<CodeListItem> = mutableListOf()
                            response.body()!!.forEach { item ->
                                list.add(item.codeListItemRequestToCodeListItem())
                            }

                            val tempArr = codeListLiveData.value?.data ?: ArrayList()
                            tempArr.addAll(list)
                            codeListLiveData.postValue(Resource.success(tempArr))

                            networkErrorLiveData.postValue("")
                        }
                    } else {
                        networkErrorLiveData.postValue(response.message())
                    }
                }

                override fun onFailure(
                    call: Call<List<CodeListItemRequest>>,
                    t: Throwable
                ) {
                    Log.v("onFailure", t.toString())

                    networkErrorLiveData.postValue("Internal Error")
                }
            })
    }

    fun getCodesListLiveData(): LiveData<Resource<ArrayList<CodeListItem>>> {
        return codeListLiveData
    }

    fun getNetworkErrorLiveData(): LiveData<String> {
        return networkErrorLiveData
    }

    fun clearList() {
        codeListLiveData.value = Resource.success(arrayListOf())
    }

    init {
        val client = getClientWithAuth()

        codeListService = getService()
            .client(client)
            .build()
            .create(CodeListService::class.java)
    }
}