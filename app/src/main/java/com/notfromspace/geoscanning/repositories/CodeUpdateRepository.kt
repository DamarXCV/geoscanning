package com.notfromspace.geoscanning.repositories

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.notfromspace.geoscanning.apis.CodeUpdateService
import com.notfromspace.geoscanning.models.CodeDetailsRequest
import com.notfromspace.geoscanning.models.CodeUpdate
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CodeUpdateRepository : BaseRepository() {
    private val codeUpdateService: CodeUpdateService
    private val codeDetailsLiveData: MutableLiveData<CodeUpdate> = MutableLiveData<CodeUpdate>(null)
    private var networkErrorLiveData: MutableLiveData<String> = MutableLiveData<String>("")

    fun createCode(data: CodeUpdate) {
        codeUpdateService.UpdateCode(
            data.title, data.category.id, data.location.longitude.toFloat(),
            data.location.latitude.toFloat(), data.desc, data.hint, data.id
        )
            .enqueue(object : Callback<CodeDetailsRequest> {
                override fun onResponse(
                    call: Call<CodeDetailsRequest>,
                    response: Response<CodeDetailsRequest>
                ) {
                    Log.v("onResponse", response.toString())
                    if (response.isSuccessful) {

                    } else {
                        networkErrorLiveData.postValue(response.message())
                    }
                }

                override fun onFailure(
                    call: Call<CodeDetailsRequest>,
                    t: Throwable
                ) {
                    Log.v("onFailure", t.toString())
                    networkErrorLiveData.postValue("Internal Error")
                }
            })
    }

    fun getCodeLiveData(): MutableLiveData<CodeUpdate> {
        return codeDetailsLiveData
    }

    fun getNetworkErrorLiveData(): LiveData<String> {
        return networkErrorLiveData
    }

    init {
        val client = getClientWithAuth()

        codeUpdateService = getService()
            .client(client)
            .build()
            .create(CodeUpdateService::class.java)
    }
}