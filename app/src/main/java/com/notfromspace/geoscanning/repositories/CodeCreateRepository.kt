package com.notfromspace.geoscanning.repositories

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.notfromspace.geoscanning.apis.CodeCreateService
import com.notfromspace.geoscanning.models.CodeCreate
import com.notfromspace.geoscanning.models.CodeDetailsRequest
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CodeCreateRepository : BaseRepository() {
    private val codeCreateService: CodeCreateService
    private val codeDetailsLiveData: MutableLiveData<CodeCreate> = MutableLiveData<CodeCreate>(null)
    private var networkErrorLiveData: MutableLiveData<String> = MutableLiveData<String>("")

    fun createCode(data: CodeCreate) {
        codeCreateService.CreateCode(
            data.title, data.category.id, data.location.longitude.toFloat(),
            data.location.latitude.toFloat(), data.desc, data.hint
        )
            .enqueue(object : Callback<CodeDetailsRequest> {
                override fun onResponse(
                    call: Call<CodeDetailsRequest>,
                    response: Response<CodeDetailsRequest>
                ) {
                    Log.v("onResponse", response.toString())
                    if (response.isSuccessful) {

                    } else {
                        networkErrorLiveData.postValue(response.message())
                    }
                }

                override fun onFailure(
                    call: Call<CodeDetailsRequest>,
                    t: Throwable
                ) {
                    Log.v("onFailure", t.toString())
                    networkErrorLiveData.postValue("Internal Error")
                }
            })
    }

    fun getCodeLiveData(): MutableLiveData<CodeCreate> {
        return codeDetailsLiveData
    }

    fun getNetworkErrorLiveData(): LiveData<String> {
        return networkErrorLiveData
    }

    init {
        val client = getClientWithAuth()

        codeCreateService = getService()
            .client(client)
            .build()
            .create(CodeCreateService::class.java)
    }
}