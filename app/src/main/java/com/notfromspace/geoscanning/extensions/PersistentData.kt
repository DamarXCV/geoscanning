package com.notfromspace.geoscanning.extensions

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.notfromspace.geoscanning.models.UserSession


class PersistentData(context: Context) {
    init {
        appContext = context
        LoadUserData()
    }

    companion object {
        private lateinit var appContext: Context
        private val filename: String = "UserData"
        private val gson: Gson = Gson()
        private var userSession: UserSession? = null

        fun GetUserSession(): UserSession? {
            if (userSession == null) {
                LoadUserData()
            }
            return userSession
        }

        fun DeleteUserData() {
            appContext.deleteFile(filename)
            userSession = null
        }

        fun SaveUserData(data: UserSession) {
            userSession = data

            try {
                appContext.openFileOutput(filename, Context.MODE_PRIVATE)
                    .use {
                        it.write(gson.toJson(userSession).toByteArray())
                    }
                Log.v("SaveUserData", "Success!")
            } catch (e: Exception) {
                Log.v("SaveUserData", "Failed!")
                e.printStackTrace()
            }
        }

        fun LoadUserData(): UserSession? {
            try {
                if (!appContext.getFileStreamPath(filename)
                        .exists()
                ) throw java.lang.Exception("File does not exist!")

                val string = appContext.openFileInput(filename).bufferedReader()
                    .useLines { lines ->
                        lines.fold("") { some, text ->
                            some + text
                        }
                    }
                Log.v("LoadUserData", "Success!")

                userSession = gson.fromJson(string, UserSession::class.java)

                return userSession
            } catch (e: Exception) {
                Log.v("LoadUserData", "Failed!")
                e.printStackTrace()
            }
            return null
        }
    }
}