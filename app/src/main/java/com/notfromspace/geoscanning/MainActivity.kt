package com.notfromspace.geoscanning

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView
import com.notfromspace.geoscanning.extensions.PersistentData
import com.notfromspace.geoscanning.fragments.home.HomeFragmentDirections


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var navController: NavController
    private lateinit var toolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        PersistentData(applicationContext)

        // set view
        setContentView(R.layout.activity_main)


        // set toolbar (topBar)
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        // set drawer
        drawerLayout = findViewById(R.id.drawer_layout)

        val navView: NavigationView = findViewById(R.id.nav_view)
        navController = findNavController(R.id.nav_host_fragment)

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        // prevent drawer gesture if not on start destination
        navController.addOnDestinationChangedListener { nc: NavController, nd: NavDestination, args: Bundle? ->
            // enable the drawer just on the start destination
            if (nd.id == nc.graph.startDestination) {
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
            } else {
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
            }

            // hide the default toolbar for the fragment_code_details and profile fragment
            if (
                nd.id == R.id.nav_code_details
                || nd.id == R.id.nav_profile
            ) {
                toolbar.visibility = View.GONE
            } else {
                toolbar.visibility = View.VISIBLE
            }
        }

        navView.setNavigationItemSelectedListener(fun(item): Boolean {
            when (item.itemId) {
                R.id.nav_profile -> {
                    val id = PersistentData.GetUserSession()?.userId ?: 1
                    Log.v("userId", id.toString())
                    val action = HomeFragmentDirections.actionNavHomeToNavProfile(id)
                    navController.navigate(action)
                }
                R.id.nav_bookmarks -> {
                    navController.navigate(R.id.action_nav_home_to_nav_bookmarks)
                }
                R.id.nav_settings -> {
                    navController.navigate(R.id.action_nav_home_to_nav_settings)

                }
                R.id.nav_help -> {
                    navController.navigate(R.id.action_nav_home_to_nav_help)

                }
                R.id.nav_about -> {
                    navController.navigate(R.id.action_nav_home_to_nav_about)
                }
                R.id.drawer_logout -> {
                    PersistentData.DeleteUserData()
                    startActivity(Intent(this, AuthActivity::class.java))
                }
                else -> {

                }
            }
            //close navigation drawer
            drawerLayout.closeDrawer(GravityCompat.START)

            return true
        })
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_profile -> {
                val id = PersistentData.GetUserSession()?.userId ?: 1
                Log.v("userId", id.toString())
                val action = HomeFragmentDirections.actionNavHomeToNavProfile(id)
                navController.navigate(action)
            }
            R.id.nav_bookmarks -> {
                navController.navigate(R.id.action_nav_home_to_nav_bookmarks)
            }
            R.id.nav_settings -> {
                navController.navigate(R.id.action_nav_home_to_nav_settings)

            }
            R.id.nav_help -> {
                navController.navigate(R.id.action_nav_home_to_nav_help)

            }
            R.id.nav_about -> {
                navController.navigate(R.id.action_nav_home_to_nav_about)
            }
            R.id.drawer_logout -> {
                PersistentData.DeleteUserData()
                startActivity(Intent(this, AuthActivity::class.java))
            }
            else -> {

            }
        }
        //close navigation drawer
        drawerLayout.closeDrawer(GravityCompat.START)

        return true
    }

}
