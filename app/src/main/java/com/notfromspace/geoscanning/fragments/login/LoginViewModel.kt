package com.notfromspace.geoscanning.fragments.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.notfromspace.geoscanning.models.UserLogin
import com.notfromspace.geoscanning.models.UserSession
import com.notfromspace.geoscanning.repositories.UserLoginRepository

class LoginViewModel : ViewModel() {
    private var userCreateRepository: UserLoginRepository = UserLoginRepository()
    private var userLiveData: LiveData<UserSession> = userCreateRepository.getUserLiveData()
    private var networkErrorLiveData: LiveData<String> =
        userCreateRepository.getNetworkErrorLiveData()

    var data: UserLogin = UserLogin()

    fun createUser() {
        userCreateRepository.Login(data)
    }

    fun getUserLiveData(): LiveData<UserSession> {
        return userLiveData
    }

    fun getNetworkErrorLiveData(): LiveData<String> {
        return networkErrorLiveData
    }
}