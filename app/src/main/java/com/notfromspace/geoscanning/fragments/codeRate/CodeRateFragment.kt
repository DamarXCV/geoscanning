package com.notfromspace.geoscanning.fragments.codeRate

import android.content.Context
import android.os.Bundle
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import com.notfromspace.geoscanning.R
import com.notfromspace.geoscanning.databinding.FragmentCodeRateBinding


class CodeRateFragment : Fragment() {
    val args: CodeRateFragmentArgs by navArgs()

    private lateinit var viewModel: CodeRateViewModel
    private lateinit var navigationController: NavController
    private lateinit var binding: FragmentCodeRateBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(CodeRateViewModel::class.java)
        viewModel.logCode(args.codeKey)


        binding = FragmentCodeRateBinding.inflate(
            inflater,
            container,
            false
        )
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navigationController = Navigation.findNavController(view)
    }

    override fun onCreateOptionsMenu(menu: Menu, menuInflater: MenuInflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.fragment_code_rate, menu)

        menu.findItem(R.id.action_save).setOnMenuItemClickListener(fun(item: MenuItem): Boolean {
            when (item.itemId) {
                R.id.action_save -> {
                    viewModel.rateCode()
                    Toast.makeText(
                        requireContext(),
                        "You rated the code successful!",
                        Toast.LENGTH_LONG
                    ).show()
                    navigationController.popBackStack(R.id.nav_home, false)
                    return false
                }
            }

            return false
        })
    }

    override fun onStop() {
        super.onStop()
        val inputMethodManager = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view?.applicationWindowToken, 0)
    }
}