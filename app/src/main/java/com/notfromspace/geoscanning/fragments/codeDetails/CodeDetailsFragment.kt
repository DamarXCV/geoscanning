package com.notfromspace.geoscanning.fragments.codeDetails

import android.Manifest
import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.Button
import android.widget.FrameLayout
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.ui.setupWithNavController
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.google.zxing.integration.android.IntentIntegrator
import com.notfromspace.geoscanning.BuildConfig
import com.notfromspace.geoscanning.R
import com.notfromspace.geoscanning.databinding.FragmentCodeDetailsBinding
import com.notfromspace.geoscanning.extensions.MapStateManager
import com.notfromspace.geoscanning.extensions.PersistentData
import com.notfromspace.geoscanning.models.CodeDetails
import kotlinx.android.synthetic.main.fragment_code_details.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.File


class CodeDetailsFragment : Fragment(), OnMapReadyCallback {

    private val DOWNLOAD_FILE_CODE = 2

    private val CAMERA = Manifest.permission.CAMERA
    private val CAMERA_PERMISSION_REQUEST_CODE = 12345
    private val File_PERMISSION_REQUEST_CODE = 1234567

    private lateinit var viewModel: CodeDetailsViewModel
    private lateinit var mapView: MapView
    private lateinit var googleMap: GoogleMap
    private lateinit var mapStateManager: MapStateManager
    private lateinit var navigationController: NavController
    private lateinit var binding: FragmentCodeDetailsBinding

    private lateinit var progressBar: FrameLayout
    private lateinit var bookmark: MenuItem

    val args: CodeDetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(CodeDetailsViewModel::class.java)
        viewModel.getCodeLiveData().observe(viewLifecycleOwner, Observer<CodeDetails> { code ->
            if (this::googleMap.isInitialized && code != null) {
                val target = LatLng(code.lat, code.long)
                val update: CameraUpdate =
                    CameraUpdateFactory.newCameraPosition(CameraPosition(target, 15f, 0f, 0f))
                googleMap.moveCamera(update)
                setMarker(target)
            }

            if (code != null && code.isBookmarked) {
                bookmark.icon = ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_bookmark_24,
                    resources.newTheme()
                )
            } else {
                bookmark.icon = ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_baseline_bookmark_border_24,
                    resources.newTheme()
                )
            }
        })
        viewModel.code(args.codeId)
        viewModel.getNetworkErrorLiveData().observe(viewLifecycleOwner, Observer<String> { error ->
            if (error != "") {
                Toast.makeText(requireContext(), error, Toast.LENGTH_LONG).show()
                viewModel.resetNetworkErrorLiveData()
            }
        })

        binding = FragmentCodeDetailsBinding.inflate(
            inflater,
            container,
            false
        )

        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        progressBar = view.findViewById(R.id.progressBar)
        viewModel.getCodeLiveData().observe(viewLifecycleOwner, Observer<CodeDetails> { code ->
            if (code == null) {
                progressBar.visibility = View.VISIBLE
            } else {
                if (viewModel.getCodeLiveData().value?.ownerId != PersistentData.GetUserSession()?.userId) {
                    toolbar.menu.findItem(R.id.action_get_qr).isVisible = false
                    toolbar.menu.findItem(R.id.action_edit_code).isVisible = false
                }
                progressBar.visibility = View.GONE
            }
        })
        viewModel.getQRCodeLiveData().observe(viewLifecycleOwner, Observer<String> { qrCode ->
            if (qrCode != null && viewModel.uriLiveData.value != null) {
                context?.contentResolver?.openOutputStream(viewModel.uriLiveData.value!!)
                    ?.let { outputStream ->
                        CoroutineScope(Dispatchers.IO).launch {
                            val decodedString: ByteArray =
                                Base64.decode(qrCode, Base64.DEFAULT)

                            // Bitmap Image
                            val bitmap: Bitmap =
                                BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)

                            try {
                                bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream)
                                outputStream.flush()
                                outputStream.close()
//                            viewFile(viewModel.uriLiveData.value!!)
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    }
            }
        })

        val code = viewModel.getCodeLiveData().value
        if (code == null) {
            progressBar.visibility = View.VISIBLE
        } else {
            progressBar.visibility = View.GONE
        }

        navigationController = Navigation.findNavController(view)

        collapsingToolbar.setupWithNavController(toolbar, findNavController())

        toolbar.inflateMenu(R.menu.fragment_code_details)
        bookmark = toolbar.menu.findItem(R.id.action_bookmark)

        toolbar.setOnMenuItemClickListener(fun(item: MenuItem): Boolean {
            when (item.itemId) {
                R.id.action_history -> {
                    val action =
                        CodeDetailsFragmentDirections.actionNavCodeDetailsToNavCodeHistory(args.codeId)
                    navigationController.navigate(action)
                    return false
                }
                R.id.action_bookmark -> {
                    viewModel.toggleBookmark()
                }
                R.id.action_get_qr -> {
                    if (checkAndGetFilePermission()) {
                        setDownloadDir()
                    }
                }
                R.id.action_edit_code -> {
                    val action = CodeDetailsFragmentDirections.actionNavCodeDetailsToNavCodeUpdate(
                        viewModel.getCodeLiveData().value!!
                    )
                    navigationController.navigate(action)
                }
            }

            return false
        })

        val textViewName: View = view.findViewById(R.id.textView_hidden_from_name)
        textViewName.setOnClickListener(fun(view: View) {
            val action =
                CodeDetailsFragmentDirections.actionNavCodeDetailsToNavProfile(viewModel.getCodeLiveData().value?.ownerId!!)
            navigationController.navigate(action)
        })

        val buttonNavigate: Button = view.findViewById(R.id.button_navigate)
        buttonNavigate.setOnClickListener(fun(_: View) {
            val uri =
                "http://maps.google.com/maps?z=12&t=m&q=loc:${viewModel.getCodeLiveData().value?.lat.toString()}+${viewModel.getCodeLiveData().value?.long}"
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
            startActivity(intent)
        })

        val buttonLogging: Button = view.findViewById(R.id.button_logging)
        buttonLogging.setOnClickListener(fun(_: View) {
            if (checkAndGetCameraPermission()) {
                initScan()
            }
        })

        mapStateManager =
            MapStateManager(
                requireContext()
            )

        mapView = view.findViewById(R.id.mapView) as MapView
        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync(this)
    }

    override fun onResume() {
        super.onResume()

        viewModel.code(args.codeId)
    }

    override fun onMapReady(map: GoogleMap) {
        googleMap = map

        @SuppressLint("MissingPermission")
        googleMap.isMyLocationEnabled = false
        googleMap.uiSettings.isMyLocationButtonEnabled = false
        googleMap.uiSettings.isMapToolbarEnabled = false
        googleMap.uiSettings.isZoomControlsEnabled = false
        googleMap.uiSettings.isScrollGesturesEnabled = false
        googleMap.uiSettings.isRotateGesturesEnabled = false
        googleMap.uiSettings.isTiltGesturesEnabled = false
        googleMap.uiSettings.isZoomGesturesEnabled = false

        googleMap.mapType = mapStateManager.savedMapType

        val codeDetails: CodeDetails? = viewModel.getCodeLiveData().value
        if (codeDetails != null) {
            val target = LatLng(codeDetails.lat, codeDetails.long)
            val update: CameraUpdate =
                CameraUpdateFactory.newCameraPosition(CameraPosition(target, 15f, 0f, 0f))
            googleMap.moveCamera(update)
            setMarker(target)
        }

        mapView.onResume()
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        val result =
            IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            if (result.contents == null) {
//                Toast.makeText(requireContext(), "Cancelled", Toast.LENGTH_LONG).show()
            } else {
//                Toast.makeText(requireContext(), "Scanned: " + result.contents, Toast.LENGTH_LONG).show()
                val action =
                    CodeDetailsFragmentDirections.actionNavCodeDetailsToNavCodeRate(result.contents)
                navigationController.navigate(action)
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }

        if (requestCode == DOWNLOAD_FILE_CODE && resultCode == Activity.RESULT_OK) {
            data?.data?.let { uri ->
                context?.let {
                    viewModel.getQRCode()
                    viewModel.uriLiveData.postValue(uri)
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            CAMERA_PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty()) {
                    var i = 0
                    while (i < grantResults.size) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            return
                        }
                        i++
                    }
                    // Has permissions
//                    initScan()
                }
            }
            File_PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty()) {
                    var i = 0
                    while (i < grantResults.size) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            return
                        }
                        i++
                    }
                    // Has permissions
//                    setDownloadDir()
                }
            }
        }
    }

    private fun setDownloadDir() {
        val folder = context?.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
        val fileName = viewModel.getCodeLiveData().value?.id + ".png"
        val file = File(folder, fileName)
        val uri = context?.let {
            FileProvider.getUriForFile(it, "${BuildConfig.APPLICATION_ID}.provider", file)
        }
        val extension = MimeTypeMap.getFileExtensionFromUrl(uri?.path)
        val mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)

        val intent = Intent(Intent.ACTION_CREATE_DOCUMENT)
        intent.setDataAndType(uri, mimeType)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        intent.putExtra(Intent.EXTRA_TITLE, fileName)
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        startActivityForResult(intent, DOWNLOAD_FILE_CODE)
    }

    private fun checkAndGetFilePermission(): Boolean {
        val permissions = arrayOf(
            READ_EXTERNAL_STORAGE,
            WRITE_EXTERNAL_STORAGE
        )

        if ((ContextCompat.checkSelfPermission(
                requireContext().applicationContext,
                READ_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED)
            && ContextCompat.checkSelfPermission(
                requireContext().applicationContext,
                WRITE_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        } else {
            requestPermissions(
                permissions,
                CAMERA_PERMISSION_REQUEST_CODE
            )
            return false
        }
    }

    private fun checkAndGetCameraPermission(): Boolean {
        val permissions = arrayOf(
            CAMERA
        )

        if (ContextCompat.checkSelfPermission(
                requireContext().applicationContext,
                CAMERA
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        } else {
            requestPermissions(
                permissions,
                CAMERA_PERMISSION_REQUEST_CODE
            )
            return false
        }
    }

    private fun initScan() {
        val integrator: IntentIntegrator = IntentIntegrator.forSupportFragment(this)
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE)
        integrator.setPrompt("")
        integrator.setCameraId(0)
        integrator.setBeepEnabled(false)
        integrator.setBarcodeImageEnabled(false)
        integrator.initiateScan()
    }

    private fun setMarker(coords: LatLng) {
        val markerIcon = bitmapDescriptorFromVector(requireContext(), R.drawable.ic_pointer)

        googleMap.addMarker(
            MarkerOptions()
                .position(coords)
                .icon(markerIcon)
        )
    }

    private fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor {
        val vectorDrawable: Drawable? = ContextCompat.getDrawable(context, vectorResId)
        vectorDrawable!!.setBounds(
            0,
            0,
            vectorDrawable.intrinsicWidth,
            vectorDrawable.intrinsicHeight
        )
        val bitmap = Bitmap.createBitmap(
            vectorDrawable.intrinsicWidth,
            vectorDrawable.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }
}