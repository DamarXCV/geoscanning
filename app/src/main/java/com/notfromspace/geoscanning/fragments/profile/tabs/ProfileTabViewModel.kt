package com.notfromspace.geoscanning.fragments.profile.tabs

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.notfromspace.geoscanning.models.User
import com.notfromspace.geoscanning.repositories.UserRepository

class ProfileTabViewModel : ViewModel() {
    private var userRepository: UserRepository = UserRepository()
    private var userLiveData: LiveData<User> = userRepository.getUserLiveData()
    private var networkErrorLiveData: LiveData<String> = userRepository.getNetworkErrorLiveData()

    fun user(codeId: Int) {
        userRepository.getUser(codeId)
    }

    fun getCodeLiveData(): LiveData<User> {
        return userLiveData
    }

    fun getNetworkErrorLiveData(): LiveData<String> {
        return networkErrorLiveData
    }
}
