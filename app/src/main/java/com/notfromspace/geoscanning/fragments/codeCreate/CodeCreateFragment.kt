package com.notfromspace.geoscanning.fragments.codeCreate

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.notfromspace.geoscanning.R
import com.notfromspace.geoscanning.databinding.FragmentCodeCreateBinding

class CodeCreateFragment : Fragment(), OnMapReadyCallback {
    private val FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION
    private val COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION
    private val LOCATION_PERMISSION_REQUEST_CODE = 1234

    private lateinit var viewModel: CodeCreateViewModel
    private lateinit var navigationController: NavController
    private lateinit var binding: FragmentCodeCreateBinding

    private lateinit var mapView: MapView
    private lateinit var googleMap: GoogleMap

    private var gpsAlert: AlertDialog? = null
    private var locationManager: LocationManager? = null
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(CodeCreateViewModel::class.java)
        val view = inflater.inflate(R.layout.fragment_code_create, container, false)

        locationManager =
            requireContext().applicationContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager?

        binding = FragmentCodeCreateBinding.inflate(
            inflater,
            container,
            false
        )
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navigationController = Navigation.findNavController(view)

        mapView = view.findViewById(R.id.mapView) as MapView
        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync(this)
    }

    override fun onStop() {
        super.onStop()
        val inputMethodManager =
            context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view?.applicationWindowToken, 0)
    }

    override fun onDestroyView() {
        super.onDestroyView()

        if (gpsAlert !== null && gpsAlert!!.isShowing) gpsAlert!!.dismiss()
    }

    override fun onCreateOptionsMenu(menu: Menu, menuInflater: MenuInflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.fragment_code_create, menu)

        menu.findItem(R.id.action_save).setOnMenuItemClickListener(fun(item: MenuItem): Boolean {
            when (item.itemId) {
                R.id.action_save -> {
                    viewModel.createCode()
                    Toast.makeText(
                        requireContext(),
                        "You created the code successful!",
                        Toast.LENGTH_LONG
                    ).show()
                    navigationController.popBackStack(R.id.nav_home, false)
                    return false
                }
            }

            return false
        })
    }

    override fun onMapReady(map: GoogleMap) {
        googleMap = map

        googleMap.setOnCameraIdleListener {
            viewModel.data.location = googleMap.projection.visibleRegion.latLngBounds.center
        }

        @SuppressLint("MissingPermission")
        googleMap.isMyLocationEnabled = false
        googleMap.uiSettings.isMyLocationButtonEnabled = false
        googleMap.uiSettings.isMapToolbarEnabled = false
        googleMap.uiSettings.isZoomControlsEnabled = true
        googleMap.uiSettings.isScrollGesturesEnabled = true
        googleMap.uiSettings.isRotateGesturesEnabled = true
        googleMap.uiSettings.isTiltGesturesEnabled = true
        googleMap.uiSettings.isZoomGesturesEnabled = true

        mapView.onResume()

        goToMyLocation()
    }

    private fun checkAndGetLocationPermission(): Boolean {
        val permissions = arrayOf(
            FINE_LOCATION,
            COURSE_LOCATION
        )

        if (ContextCompat.checkSelfPermission(
                requireContext().applicationContext,
                FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
            && ContextCompat.checkSelfPermission(
                requireContext().applicationContext,
                COURSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        } else {
            requestPermissions(
                permissions,
                LOCATION_PERMISSION_REQUEST_CODE
            )
            return false
        }
    }

    private fun buildAlertMessageNoGps() {
        val builder = AlertDialog.Builder(requireContext())
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
            .setCancelable(false)
            .setPositiveButton(
                "Yes"
            ) { _, _ -> startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)) }
            .setNegativeButton(
                "No"
            ) { dialog, _ -> dialog.cancel() }
        gpsAlert = builder.create()
        gpsAlert!!.show()
    }

    private fun goToMyLocation() {
        if (!checkAndGetLocationPermission()) return

        if (!locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER))
            buildAlertMessageNoGps()

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())

        fusedLocationClient.lastLocation
            .addOnSuccessListener { location ->
                // Got last known location. In some rare situations, this can be null.
                if (location != null) {
                    val position: CameraPosition? = CameraPosition.fromLatLngZoom(
                        LatLng(location.latitude, location.longitude),
                        16.0f
                    )
                    val update: CameraUpdate = CameraUpdateFactory.newCameraPosition(position)

                    googleMap.moveCamera(update)
                }
            }
    }
}