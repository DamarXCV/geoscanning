package com.notfromspace.geoscanning.fragments.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.notfromspace.geoscanning.databinding.FragmentSettingsBinding
import com.notfromspace.geoscanning.extensions.PersistentData

class SettingsFragment : Fragment() {

    private lateinit var viewModel: SettingsViewModel
    private lateinit var binding: FragmentSettingsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(SettingsViewModel::class.java)
        viewModel.getUser(PersistentData.GetUserSession()?.userId!!)

        binding = FragmentSettingsBinding.inflate(
            inflater,
            container,
            false
        )
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        viewModel.getUserOldLiveData().observe(viewLifecycleOwner, Observer {
            if (it != null) {
                viewModel.setValues(it)
                binding.invalidateAll()
            }
        })

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.getUserLiveData().observe(viewLifecycleOwner, Observer {
            if (it != null) {
                Toast.makeText(requireContext(), "Change Successful!", Toast.LENGTH_LONG).show()
            }
        })

        viewModel.getNetworkErrorLiveData().observe(viewLifecycleOwner, Observer<String> { error ->
            if (error != "") {
                Toast.makeText(requireContext(), error, Toast.LENGTH_LONG).show()
            }
        })
    }
}