package com.notfromspace.geoscanning.fragments.codeHistory

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.notfromspace.geoscanning.models.CodeRating
import com.notfromspace.geoscanning.repositories.CodeRatingListRepository
import com.yogeshpaliyal.universal_adapter.utils.Resource
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.time.LocalDateTime

class CodeHistoryViewModel : ViewModel() {
    private var codeRatingListRepository: CodeRatingListRepository = CodeRatingListRepository()
    private var codesRatingListLiveData = codeRatingListRepository.getCodeRatingListLiveData()
    private var networkErrorLiveData: LiveData<String> =
        codeRatingListRepository.getNetworkErrorLiveData()

    var fetchJob: Job? = null

    fun getList(codeId: String, beforeDate: LocalDateTime, startPosition: Int, numberOfItems: Int) {
        fetchJob = viewModelScope.launch {
            codeRatingListRepository.getCodeRatingList(
                codeId,
                beforeDate,
                startPosition,
                numberOfItems
            )
        }
    }

    fun getCodeRatingListLiveData(): LiveData<Resource<ArrayList<CodeRating>>> {
        return codesRatingListLiveData
    }

    fun getNetworkErrorLiveData(): LiveData<String> {
        return networkErrorLiveData
    }
}
