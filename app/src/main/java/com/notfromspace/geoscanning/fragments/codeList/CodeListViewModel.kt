package com.notfromspace.geoscanning.fragments.codeList

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.notfromspace.geoscanning.models.CodeListItem
import com.notfromspace.geoscanning.repositories.CodeListRepository
import com.yogeshpaliyal.universal_adapter.utils.Resource
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class CodeListViewModel : ViewModel() {
    private var codeListRepository: CodeListRepository = CodeListRepository()
    private var codesListLiveData = codeListRepository.getCodesListLiveData()
    private var networkErrorLiveData: LiveData<String> =
        codeListRepository.getNetworkErrorLiveData()

    var fetchJob: Job? = null

    fun getCreatedList(userId: Int, beforeDate: String, startPosition: Int, numberOfItems: Int) {
        fetchJob = viewModelScope.launch {
            codeListRepository.getCreatedList(userId, beforeDate, startPosition, numberOfItems)
        }
    }

    fun getFoundList(userId: Int, beforeDate: String, startPosition: Int, numberOfItems: Int) {
        fetchJob = viewModelScope.launch {
            codeListRepository.getFoundList(userId, beforeDate, startPosition, numberOfItems)
        }
    }

    fun getBookmarkList(userId: Int, beforeDate: String, startPosition: Int, numberOfItems: Int) {
        fetchJob = viewModelScope.launch {
            codeListRepository.getBookmarkList(userId, beforeDate, startPosition, numberOfItems)
        }
    }

    fun clearList() {
        codeListRepository.clearList()
    }

    fun getCodeLiveData(): LiveData<Resource<ArrayList<CodeListItem>>> {
        return codesListLiveData
    }

    fun getNetworkErrorLiveData(): LiveData<String> {
        return networkErrorLiveData
    }
}