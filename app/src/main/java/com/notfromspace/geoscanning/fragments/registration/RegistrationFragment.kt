package com.notfromspace.geoscanning.fragments.registration

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.notfromspace.geoscanning.MainActivity
import com.notfromspace.geoscanning.databinding.FragmentRegistrationBinding
import com.notfromspace.geoscanning.extensions.PersistentData

class RegistrationFragment : Fragment() {
    private lateinit var viewModel: RegistrationViewModel
    private lateinit var binding: FragmentRegistrationBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(RegistrationViewModel::class.java)

        binding = FragmentRegistrationBinding.inflate(
            inflater,
            container,
            false
        )
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.getUserLiveData().observe(viewLifecycleOwner, Observer {
            if (it != null && PersistentData.GetUserSession() != null) {
                startActivity(Intent(context, MainActivity::class.java))
            }
        })

        viewModel.getNetworkErrorLiveData().observe(viewLifecycleOwner, Observer<String> { error ->
            if (error != "") {
                Toast.makeText(requireContext(), error, Toast.LENGTH_LONG).show()
            }
        })
    }

}