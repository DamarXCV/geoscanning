package com.notfromspace.geoscanning.fragments.help

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.notfromspace.geoscanning.R

class ExpandableListAdapter(var expandableList: List<ExpandableItem>) :
    RecyclerView.Adapter<ExpandableListAdapter.ExpandableListVH>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ExpandableListVH {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.expandable_item, parent, false)
        return ExpandableListVH(
            view
        )
    }

    override fun onBindViewHolder(
        holder: ExpandableListVH,
        position: Int
    ) {
        val expandableItem = expandableList[position]
        holder.questionText.text = expandableItem.question
        holder.answerText.text = expandableItem.answer
        val isExpandable = expandableList[position].isExpandable
        holder.relativeLayout.visibility = if (isExpandable) View.VISIBLE else View.GONE

        val icon =
            if (expandableItem.isExpandable) R.drawable.ic_baseline_keyboard_arrow_up_24 else R.drawable.ic_baseline_keyboard_arrow_down_24
        holder.questionText.setCompoundDrawablesWithIntrinsicBounds(0, 0, icon, 0)
    }

    override fun getItemCount(): Int {
        return expandableList.size
    }

    inner class ExpandableListVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var questionText: TextView = itemView.findViewById(R.id.question)
        var answerText: TextView = itemView.findViewById(R.id.answer)
        var linearLayout: LinearLayout = itemView.findViewById(R.id.linear_layout)
        var relativeLayout: RelativeLayout = itemView.findViewById(R.id.expandable_layout)

        init {


            linearLayout.setOnClickListener { view: View? ->
                val expandableItem = expandableList[adapterPosition]
                expandableItem.isExpandable = !expandableItem.isExpandable
                val icon =
                    if (expandableItem.isExpandable) R.drawable.ic_baseline_keyboard_arrow_up_24 else R.drawable.ic_baseline_keyboard_arrow_down_24
                view?.findViewById<TextView>(R.id.question)
                    ?.setCompoundDrawablesWithIntrinsicBounds(0, 0, icon, 0)
                notifyItemChanged(adapterPosition)
            }
        }
    }

}