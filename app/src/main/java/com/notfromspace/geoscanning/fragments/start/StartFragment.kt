package com.notfromspace.geoscanning.fragments.start

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.notfromspace.geoscanning.R


class StartFragment : Fragment() {
    private lateinit var navigationController: NavController

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_start, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navigationController = Navigation.findNavController(view)

        navigationController = Navigation.findNavController(view)

        val loginBtn: Button = view.findViewById(R.id.loginButton)
        loginBtn.setOnClickListener {
            navigationController.navigate(R.id.action_nav_start_to_nav_login)
        }

        val registrationBtn: Button = view.findViewById(R.id.registrationButton)
        registrationBtn.setOnClickListener {
            navigationController.navigate(R.id.action_nav_start_to_nav_registration)
        }
    }
}