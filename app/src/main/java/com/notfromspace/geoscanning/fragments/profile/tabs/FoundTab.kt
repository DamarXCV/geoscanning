package com.notfromspace.geoscanning.fragments.profile.tabs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.notfromspace.geoscanning.R

class FoundTab(private var userId: Int) : Fragment() {

    private lateinit var viewModel: FoundTabViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(FoundTabViewModel::class.java)
        val view: View = inflater.inflate(R.layout.fragment_tab_found, container, false)

        return view
    }

    fun userId(): Int {
        return userId
    }
}
