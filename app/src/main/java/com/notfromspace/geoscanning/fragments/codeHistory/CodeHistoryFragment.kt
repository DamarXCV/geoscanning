package com.notfromspace.geoscanning.fragments.codeHistory

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.notfromspace.geoscanning.R
import com.notfromspace.geoscanning.extensions.BasicListener
import com.notfromspace.geoscanning.initViewModel
import com.notfromspace.geoscanning.models.CodeRating
import com.techpaliyal.androidkotlinmvvm.extensions.setupPagination
import com.yogeshpaliyal.universal_adapter.adapter.UniversalAdapterViewType
import com.yogeshpaliyal.universal_adapter.utils.Resource
import com.yogeshpaliyal.universal_adapter.utils.Status
import com.yogeshpaliyal.universal_adapter.utils.UniversalAdapterBuilder
import java.time.LocalDateTime


class CodeHistoryFragment : Fragment() {

    private val args: CodeHistoryFragmentArgs by navArgs()
    private val currentTime: LocalDateTime = LocalDateTime.now()

    private val mViewModel by lazy {
        initViewModel(CodeHistoryViewModel::class.java)
    }

    private val mAdapter by lazy {
        UniversalAdapterBuilder<CodeRating>(
            null,
            null,
            UniversalAdapterViewType.Content<CodeRating>(R.layout.item_code_history_list,
                listener = object :
                    BasicListener<CodeRating> {
                    override fun onClick(model: CodeRating) {

                    }
                }),
            UniversalAdapterViewType.Loading<CodeRating>(resourceLoading = R.layout.layout_loading_full_page),
            UniversalAdapterViewType.LoadingFooter<CodeRating>(loaderFooter = R.layout.item_loading_more),
            UniversalAdapterViewType.NoData<CodeRating>(),
            UniversalAdapterViewType.Error<CodeRating>()
        ).build()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view: View = inflater.inflate(R.layout.fragment_code_history, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val listView: RecyclerView = view.findViewById(R.id.list_history)

        listView.layoutManager = LinearLayoutManager(this.context)
//        listView.setHasFixedSize(true)
        listView.adapter = mAdapter

        listView.setupPagination {
            if (mViewModel.fetchJob?.isActive == false) {
                var startPos = mViewModel.getCodeRatingListLiveData().value?.data?.size
                if (startPos == null) {
                    startPos = 0
                }
                mViewModel.getList(args.codeId, currentTime, startPos, 10)
            }
        }

        mViewModel.getCodeRatingListLiveData().observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                }
                Status.SUCCESS -> {
                    mAdapter.updateData(Resource.success(it.data))
                }
                else -> { }
            }
            mAdapter.notifyDataSetChanged()
        })

        mViewModel.getNetworkErrorLiveData()
            ?.observe(viewLifecycleOwner, Observer<String> { error ->
                if (error != "") {
                    Toast.makeText(requireContext(), error, Toast.LENGTH_LONG).show()
                }
            })

        mViewModel.getList(args.codeId, currentTime, 0, 10)
    }
}