package com.notfromspace.geoscanning.fragments.registration

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.notfromspace.geoscanning.models.UserCreate
import com.notfromspace.geoscanning.models.UserSession
import com.notfromspace.geoscanning.repositories.UserCreateRepository

class RegistrationViewModel : ViewModel() {
    private var userCreateRepository: UserCreateRepository = UserCreateRepository()
    private var userLiveData: LiveData<UserSession> = userCreateRepository.getUserLiveData()
    private var networkErrorLiveData: LiveData<String> =
        userCreateRepository.getNetworkErrorLiveData()

    var data: UserCreate = UserCreate()

    fun createUser() {
        userCreateRepository.createUser(data)
    }

    fun getUserLiveData(): LiveData<UserSession> {
        return userLiveData
    }

    fun getNetworkErrorLiveData(): LiveData<String> {
        return networkErrorLiveData
    }
}