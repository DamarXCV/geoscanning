package com.notfromspace.geoscanning.fragments.codeRate

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.notfromspace.geoscanning.models.CreateCodeRating
import com.notfromspace.geoscanning.repositories.CodeRatingRepository

class CodeRateViewModel : ViewModel() {
    private var codeRateRepository: CodeRatingRepository = CodeRatingRepository()
    private var networkErrorLiveData: LiveData<String> =
        codeRateRepository.getNetworkErrorLiveData()

    var data: CreateCodeRating = CreateCodeRating(null)

    fun rateCode() {
        codeRateRepository.rateCode(data)
    }

    fun logCode(key: String) {
        codeRateRepository.logCode(key)
    }

    fun getNetworkErrorLiveData(): LiveData<String> {
        return networkErrorLiveData
    }
}