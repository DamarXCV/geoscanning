package com.notfromspace.geoscanning.fragments.codeCreate

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.maps.model.LatLng
import com.notfromspace.geoscanning.models.*
import com.notfromspace.geoscanning.repositories.CodeCreateRepository

class CodeCreateViewModel : ViewModel() {

    private var codeCreateRepository: CodeCreateRepository = CodeCreateRepository()
    private var codeCreateLiveData: LiveData<CodeCreate> = codeCreateRepository.getCodeLiveData()
    private var networkErrorLiveData: LiveData<String> = codeCreateRepository.getNetworkErrorLiveData()

    var data: CodeCreate = CodeCreate("", Categories[0], LatLng(0.0, 0.0), "", "")

    fun createCode() {
        codeCreateRepository.createCode(data)
    }

    fun getCodeCreateLiveData(): LiveData<CodeCreate> {
        return codeCreateLiveData
    }

    fun getNetworkErrorLiveData(): LiveData<String> {
        return networkErrorLiveData
    }
}