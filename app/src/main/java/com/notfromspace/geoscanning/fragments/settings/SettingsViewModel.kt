package com.notfromspace.geoscanning.fragments.settings

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.notfromspace.geoscanning.models.User
import com.notfromspace.geoscanning.models.UserSettings
import com.notfromspace.geoscanning.models.UserUpdate
import com.notfromspace.geoscanning.repositories.UserUpdateRepository

class SettingsViewModel : ViewModel() {
    private var userUpdateRepository: UserUpdateRepository = UserUpdateRepository()
    private var userLiveData: LiveData<User> = userUpdateRepository.getUserLiveData()
    private var userOldLiveData: LiveData<UserSettings> = userUpdateRepository.getUserOldLiveData()
    private var networkErrorLiveData: LiveData<String> =
        userUpdateRepository.getNetworkErrorLiveData()

    var data: UserUpdate = UserUpdate()

    fun getUser(userId: Int) {
        userUpdateRepository.getUser(userId)
    }

    fun updateUser() {
        userUpdateRepository.updateUser(data)
    }

    fun getUserLiveData(): LiveData<User> {
        return userLiveData
    }

    fun getUserOldLiveData(): LiveData<UserSettings> {
        return userOldLiveData
    }

    fun getNetworkErrorLiveData(): LiveData<String> {
        return networkErrorLiveData
    }

    fun setValues(user: UserSettings) {
        data.firstname = user.firstname
        data.lastname = user.lastname
        data.alias = user.alias
        data.hometown = user.hometown
        data.country = user.country
        data.email = user.email
        data.description = user.description
        data.profileImage = user.profileImage
    }
}