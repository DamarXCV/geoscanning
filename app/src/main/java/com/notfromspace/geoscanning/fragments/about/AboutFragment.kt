package com.notfromspace.geoscanning.fragments.about

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import com.notfromspace.geoscanning.R
import mehdi.sakout.aboutpage.AboutPage
import mehdi.sakout.aboutpage.Element


class AboutFragment : Fragment() {

    private lateinit var viewModel: AboutViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        @Nullable container: ViewGroup?,
        @Nullable savedInstanceState: Bundle?
    ): View? {
        val versionElement: Element = Element()
        versionElement.title = "Version 6.2"

        // https://github.com/medyo/android-about-page
        return AboutPage(context)
            .isRTL(false)
            .setDescription(getString(R.string.app_description))

            .addGroup(getString(R.string.contact_group))
            .addEmail("notfromspace.software@gmail.com", "Email")
            .addFacebook("Intel")
            .addTwitter("amd")
            .addYoutube("texasinstruments")

            .addGroup(getString(R.string.application_information_group))
            .addItem(versionElement)
            .create()
    }
}