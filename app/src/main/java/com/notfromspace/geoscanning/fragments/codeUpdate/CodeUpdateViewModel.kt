package com.notfromspace.geoscanning.fragments.codeUpdate

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.maps.model.LatLng
import com.notfromspace.geoscanning.models.CodeDetails
import com.notfromspace.geoscanning.models.CodeUpdate
import com.notfromspace.geoscanning.repositories.CodeUpdateRepository

class CodeUpdateViewModel : ViewModel() {

    private var codeUpdateRepository: CodeUpdateRepository = CodeUpdateRepository()
    private var codeUpdateLiveData: LiveData<CodeUpdate> = codeUpdateRepository.getCodeLiveData()
    private var networkErrorLiveData: LiveData<String> =
        codeUpdateRepository.getNetworkErrorLiveData()

    var data: CodeUpdate = CodeUpdate()

    fun updateCode() {
        codeUpdateRepository.createCode(data)
    }

    fun getCodeUpdateLiveData(): LiveData<CodeUpdate> {
        return codeUpdateLiveData
    }

    fun getNetworkErrorLiveData(): LiveData<String> {
        return networkErrorLiveData
    }

    fun setValues(code: CodeDetails) {
        data.id = code.id
        data.title = code.title
        data.category = code.category
        data.location = LatLng(code.lat, code.long)
        data.desc = code.desc
        data.hint = code.hint
    }
}