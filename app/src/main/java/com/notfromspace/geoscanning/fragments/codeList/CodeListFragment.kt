package com.notfromspace.geoscanning.fragments.codeList

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.notfromspace.geoscanning.R
import com.notfromspace.geoscanning.extensions.BasicListener
import com.notfromspace.geoscanning.extensions.PersistentData
import com.notfromspace.geoscanning.fragments.bookmarks.BookmarksFragment
import com.notfromspace.geoscanning.fragments.bookmarks.BookmarksFragmentDirections
import com.notfromspace.geoscanning.fragments.profile.ProfileFragmentDirections
import com.notfromspace.geoscanning.fragments.profile.tabs.CodesTab
import com.notfromspace.geoscanning.fragments.profile.tabs.FoundTab
import com.notfromspace.geoscanning.initViewModel
import com.notfromspace.geoscanning.models.CodeListItem
import com.techpaliyal.androidkotlinmvvm.extensions.setupPagination
import com.yogeshpaliyal.universal_adapter.adapter.UniversalAdapterViewType
import com.yogeshpaliyal.universal_adapter.utils.Resource
import com.yogeshpaliyal.universal_adapter.utils.Status
import com.yogeshpaliyal.universal_adapter.utils.UniversalAdapterBuilder
import java.time.LocalDateTime


class CodeListFragment : Fragment() {
    private lateinit var navigationController: NavController
    private val currentTime: LocalDateTime = LocalDateTime.now()
    private var userId: Int = PersistentData.GetUserSession()?.userId ?: 1

    private val mViewModel by lazy {
        initViewModel(CodeListViewModel::class.java)
    }

    private val mAdapter by lazy {
        UniversalAdapterBuilder<CodeListItem>(
            null,
            null,
            UniversalAdapterViewType.Content<CodeListItem>(R.layout.list_item_code,
                listener = object :
                    BasicListener<CodeListItem> {
                    override fun onClick(model: CodeListItem) {
                        var action: NavDirections? = null
                        when (parentFragment) {
                            is BookmarksFragment -> action =
                                BookmarksFragmentDirections.actionNavBookmarksToNavCodeDetails(
                                    model.id
                                )
                            is CodesTab -> action =
                                ProfileFragmentDirections.actionNavProfileToNavCodeDetails(
                                    model.id
                                )
                            is FoundTab -> action =
                                ProfileFragmentDirections.actionNavProfileToNavCodeDetails(
                                    model.id
                                )
                        }
                        if (action != null) {
                            navigationController.navigate(action)
                        }
                    }
                }),
            UniversalAdapterViewType.Loading<CodeListItem>(resourceLoading = R.layout.layout_loading_full_page),
            UniversalAdapterViewType.LoadingFooter<CodeListItem>(loaderFooter = R.layout.item_loading_more),
            UniversalAdapterViewType.NoData<CodeListItem>(),
            UniversalAdapterViewType.Error<CodeListItem>()
        ).build()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view: View = inflater.inflate(R.layout.fragment_code_list, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navigationController = Navigation.findNavController(view)

        when (this.parentFragment) {
            is FoundTab -> {
                val parentFrag: FoundTab = this.parentFragment as FoundTab
                userId = parentFrag.userId()
            }
            is CodesTab -> {
                val parentFrag: CodesTab = this.parentFragment as CodesTab
                userId = parentFrag.userId()
            }
        }

        val listView: RecyclerView = view.findViewById(R.id.code_list)

        listView.layoutManager = LinearLayoutManager(this.context)
//        listView.setHasFixedSize(true)
        listView.adapter = mAdapter

        listView.setupPagination {
            if (mViewModel.fetchJob?.isActive == false) {
                when (this.parentFragment) {
                    is BookmarksFragment -> mViewModel.getBookmarkList(
                        userId,
                        currentTimeString(),
                        mViewModel.getCodeLiveData().value?.data?.size ?: 0,
                        10
                    )
                    is CodesTab -> mViewModel.getCreatedList(
                        userId,
                        currentTimeString(),
                        mViewModel.getCodeLiveData().value?.data?.size ?: 0,
                        10
                    )
                    is FoundTab -> mViewModel.getFoundList(
                        userId,
                        currentTimeString(),
                        mViewModel.getCodeLiveData().value?.data?.size ?: 0,
                        10
                    )
                }
            }
        }

        mViewModel.getCodeLiveData().observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                }
                Status.SUCCESS -> {
                    mAdapter.updateData(Resource.success(it.data))
                    mAdapter.notifyDataSetChanged()
                }
                else -> {
                }
            }
        })

        mViewModel.getNetworkErrorLiveData().observe(
            viewLifecycleOwner,
            Observer<String> { error ->
                if (error != "") {
                    Toast.makeText(requireContext(), error, Toast.LENGTH_LONG).show()
                }
            })

        when (this.parentFragment) {
            is BookmarksFragment -> mViewModel.getBookmarkList(
                userId,
                currentTimeString(),
                mViewModel.getCodeLiveData().value?.data?.size ?: 0,
                10
            )
            is CodesTab -> mViewModel.getCreatedList(
                userId,
                currentTimeString(),
                mViewModel.getCodeLiveData().value?.data?.size ?: 0,
                10
            )
            is FoundTab -> mViewModel.getFoundList(
                userId,
                currentTimeString(),
                mViewModel.getCodeLiveData().value?.data?.size ?: 0,
                10
            )
        }
    }

    override fun onResume() {
        super.onResume()
        when (this.parentFragment) {
            is BookmarksFragment -> {
                mViewModel.clearList()
            }
        }
    }

    private fun currentTimeString(): String {
        return "${currentTime.year}-${currentTime.monthValue}-${currentTime.dayOfMonth} ${currentTime.hour}:${currentTime.minute}:${currentTime.second}"
    }
}