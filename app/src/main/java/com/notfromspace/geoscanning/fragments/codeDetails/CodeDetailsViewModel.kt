package com.notfromspace.geoscanning.fragments.codeDetails

import android.app.Application
import android.net.Uri
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.notfromspace.geoscanning.models.CodeDetails
import com.notfromspace.geoscanning.repositories.CodeDetailsRepository


class CodeDetailsViewModel(application: Application) : AndroidViewModel(application) {
    private var codeDetailsRepository: CodeDetailsRepository = CodeDetailsRepository()
    private var codeDetailsLiveData: LiveData<CodeDetails> = codeDetailsRepository.getCodeLiveData()
    private var codeDateStringLiveData: LiveData<String> =
        codeDetailsRepository.getCodeDateStringLiveData()
    private var qrCodeLiveData: LiveData<String> = codeDetailsRepository.getQRCodeLiveData()
    private var networkErrorLiveData: LiveData<String> =
        codeDetailsRepository.getNetworkErrorLiveData()

    var uriLiveData: MutableLiveData<Uri> = MutableLiveData<Uri>()

    fun code(codeId: String) {
        codeDetailsRepository.getCode(codeId)
    }

    fun toggleBookmark() {
        val isBookmarked = codeDetailsLiveData.value?.isBookmarked
        if (isBookmarked != null && isBookmarked) {
            codeDetailsRepository.removeBookmark()
        } else if (isBookmarked != null && !isBookmarked) {
            codeDetailsRepository.addBookmark()
        }
    }

    fun getQRCode() {
        codeDetailsRepository.getQRCode()
    }

    fun resetNetworkErrorLiveData() {
        codeDetailsRepository.resetNetworkErrorLiveData()
    }

    fun getCodeLiveData(): LiveData<CodeDetails> {
        return codeDetailsLiveData
    }

    fun getCodeDateStringLiveData(): LiveData<String> {
        return codeDateStringLiveData
    }

    fun getQRCodeLiveData(): LiveData<String> {
        return qrCodeLiveData
    }

    fun getNetworkErrorLiveData(): LiveData<String> {
        return networkErrorLiveData
    }
}