package com.notfromspace.geoscanning.fragments.profile.tabs

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class FoundTabViewModel : ViewModel() {
    private val _text = MutableLiveData<String>().apply {
        value = "This is found Tab"
    }
    val text: LiveData<String> = _text
}
