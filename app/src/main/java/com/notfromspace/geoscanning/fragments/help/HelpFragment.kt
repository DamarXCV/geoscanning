package com.notfromspace.geoscanning.fragments.help

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.notfromspace.geoscanning.R

class HelpFragment : Fragment() {

    private lateinit var viewModel: HelpViewModel

    private lateinit var recyclerView: RecyclerView
    private lateinit var expandableList: ArrayList<ExpandableItem>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        this.viewModel = ViewModelProvider(this).get(HelpViewModel::class.java)
        val view = inflater.inflate(R.layout.fragment_help, container, false)

        this.recyclerView = view.findViewById(R.id.list_recycler_view)

        initData()

        this.recyclerView.adapter = ExpandableListAdapter(this.expandableList)
        this.recyclerView.setHasFixedSize(true)

        return view
    }

    private fun initData() {
        this.expandableList = ArrayList<ExpandableItem>()

        this.expandableList.add(
            ExpandableItem(
                "How to create a code?",
                getString(R.string.medium_text)
            )
        )
        this.expandableList.add(
            ExpandableItem(
                "How to scan a code?",
                getString(R.string.medium_text)
            )
        )
        this.expandableList.add(
            ExpandableItem(
                "Where to i get the qr-code for my GeoScan?",
                getString(R.string.medium_text)
            )
        )
        this.expandableList.add(
            ExpandableItem(
                "How do i change my username?",
                getString(R.string.medium_text)
            )
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

    }
}
