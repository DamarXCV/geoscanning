package com.notfromspace.geoscanning.fragments.profile.tabs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.notfromspace.geoscanning.databinding.FragmentTabProfileBinding
import com.notfromspace.geoscanning.extensions.PersistentData

class ProfileTab(private var userId: Int) : Fragment() {
    private lateinit var viewModel: ProfileTabViewModel
    private lateinit var binding: FragmentTabProfileBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(ProfileTabViewModel::class.java)
        if (userId == 0 && PersistentData.GetUserSession() != null) {
            userId = PersistentData.GetUserSession()?.userId!!
        }
        viewModel.user(userId)
        viewModel.getNetworkErrorLiveData().observe(viewLifecycleOwner, Observer<String> { error ->
            if (error != "") {
                Toast.makeText(requireContext(), error, Toast.LENGTH_LONG).show()
            }
        })

        binding = FragmentTabProfileBinding.inflate(
            inflater,
            container,
            false
        )

        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        setHasOptionsMenu(true)

        return binding.root
    }
}
