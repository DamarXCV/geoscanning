package com.notfromspace.geoscanning.fragments.profile

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.notfromspace.geoscanning.fragments.profile.tabs.CodesTab
import com.notfromspace.geoscanning.fragments.profile.tabs.FoundTab
import com.notfromspace.geoscanning.fragments.profile.tabs.ProfileTab

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class SectionsPagerAdapter(private val context: Context, fa: FragmentActivity, val userId: Int) :
    FragmentStateAdapter(fa) {

    override fun createFragment(position: Int): Fragment = when (position) {
        0 -> ProfileTab(userId)
        1 -> CodesTab(userId)
        else -> FoundTab(userId)
    }

    override fun getItemCount(): Int {
        return 3
    }

}