package com.notfromspace.geoscanning.fragments.help

class ExpandableItem(var question: String, var answer: String) {
    var isExpandable = false

    override fun toString(): String {
        return "ListGenerator{" +
                "question='" + question + '\'' +
                ", answer='" + answer + '\'' +
                '}'
    }
}