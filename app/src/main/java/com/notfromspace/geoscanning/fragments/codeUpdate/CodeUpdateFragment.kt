package com.notfromspace.geoscanning.fragments.codeUpdate

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.location.LocationManager
import android.os.Bundle
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.notfromspace.geoscanning.R
import com.notfromspace.geoscanning.databinding.FragmentCodeUpdateBinding

class CodeUpdateFragment : Fragment(), OnMapReadyCallback {
    private lateinit var viewModel: CodeUpdateViewModel
    private lateinit var navigationController: NavController
    private lateinit var binding: FragmentCodeUpdateBinding

    private lateinit var mapView: MapView
    private lateinit var googleMap: GoogleMap
    private var marker: Marker? = null

    private var gpsAlert: AlertDialog? = null
    private var locationManager: LocationManager? = null

    val args: CodeUpdateFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(CodeUpdateViewModel::class.java)
        viewModel.setValues(args.codeDetails)

        locationManager =
            requireContext().applicationContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager?

        binding = FragmentCodeUpdateBinding.inflate(
            inflater,
            container,
            false
        )
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navigationController = Navigation.findNavController(view)

        mapView = view.findViewById(R.id.mapView) as MapView
        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync(this)
    }

    override fun onStop() {
        super.onStop()
        val inputMethodManager =
            context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view?.applicationWindowToken, 0)
    }

    override fun onDestroyView() {
        super.onDestroyView()

        if (gpsAlert !== null && gpsAlert!!.isShowing) gpsAlert!!.dismiss()
    }

    override fun onCreateOptionsMenu(menu: Menu, menuInflater: MenuInflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.fragment_code_update, menu)

        menu.findItem(R.id.action_save).setOnMenuItemClickListener(fun(item: MenuItem): Boolean {
            when (item.itemId) {
                R.id.action_save -> {
                    viewModel.updateCode()
                    Toast.makeText(
                        requireContext(),
                        "You updated the code successful!",
                        Toast.LENGTH_LONG
                    ).show()
                    navigationController.popBackStack(R.id.nav_home, false)
                    return false
                }
            }

            return false
        })
    }

    override fun onMapReady(map: GoogleMap) {
        googleMap = map

        googleMap.setOnCameraIdleListener {
            viewModel.data.location = googleMap.projection.visibleRegion.latLngBounds.center
        }

        @SuppressLint("MissingPermission")
        googleMap.isMyLocationEnabled = false
        googleMap.uiSettings.isMyLocationButtonEnabled = false
        googleMap.uiSettings.isMapToolbarEnabled = false
        googleMap.uiSettings.isZoomControlsEnabled = true
        googleMap.uiSettings.isScrollGesturesEnabled = true
        googleMap.uiSettings.isRotateGesturesEnabled = true
        googleMap.uiSettings.isTiltGesturesEnabled = true
        googleMap.uiSettings.isZoomGesturesEnabled = true

        mapView.onResume()

        goToLocation()
    }

    private fun goToLocation() {
        val position: CameraPosition? = CameraPosition.fromLatLngZoom(
            LatLng(viewModel.data.location.latitude, viewModel.data.location.longitude),
            16.0f
        )
        val update: CameraUpdate = CameraUpdateFactory.newCameraPosition(position)

        googleMap.moveCamera(update)
    }
}