package com.notfromspace.geoscanning.fragments.home

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener
import com.google.android.gms.maps.model.*
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.zxing.integration.android.IntentIntegrator
import com.notfromspace.geoscanning.R
import com.notfromspace.geoscanning.databinding.FragmentHomeBinding
import com.notfromspace.geoscanning.extensions.BasicListener
import com.notfromspace.geoscanning.extensions.MapStateManager
import com.notfromspace.geoscanning.models.CodeListItem
import com.notfromspace.geoscanning.models.CodeMapItem
import com.yogeshpaliyal.universal_adapter.adapter.UniversalAdapterViewType
import com.yogeshpaliyal.universal_adapter.utils.Resource
import com.yogeshpaliyal.universal_adapter.utils.Status
import com.yogeshpaliyal.universal_adapter.utils.UniversalAdapterBuilder


class HomeFragment : Fragment(), OnMapReadyCallback, OnInfoWindowClickListener {

    private val FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION
    private val COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION

    private val LOCATION_PERMISSION_REQUEST_CODE = 1234
    private val CAMERA = Manifest.permission.CAMERA
    private val CAMERA_PERMISSION_REQUEST_CODE = 12345

    private var gpsAlert: AlertDialog? = null
    private var locationManager : LocationManager? = null

    private lateinit var homeView: View
    private lateinit var homeViewModel: HomeViewModel

    private lateinit var mapView: MapView
    lateinit var googleMap: GoogleMap
    lateinit var searchButton: MenuItem
    private lateinit var searchListView: ConstraintLayout
    private lateinit var listView: RecyclerView
    private lateinit var navigationController: NavController
    private lateinit var mapStateManager: MapStateManager
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private lateinit var viewModel: HomeViewModel
    private lateinit var binding: FragmentHomeBinding

    private var searchView: SearchView? = null
    private var queryTextListener: SearchView.OnQueryTextListener? = null

    private var currentSearchTerm: String = ""

    private val markerList: HashMap<String, Boolean> = HashMap()

    private val mAdapter by lazy {
        UniversalAdapterBuilder<CodeListItem>(null,
            null,
            UniversalAdapterViewType.Content<CodeListItem>(R.layout.list_item_code,
                listener = object :
                    BasicListener<CodeListItem> {
                    override fun onClick(model: CodeListItem) {
                        val position: CameraPosition? = CameraPosition.fromLatLngZoom(
                            LatLng(model.location.latitude, model.location.longitude),
                            16.0f
                        )
                        val update: CameraUpdate = CameraUpdateFactory.newCameraPosition(position)
                        googleMap.moveCamera(update)

                        searchButton.collapseActionView()
                    }
                }),
            UniversalAdapterViewType.Loading<CodeListItem>(resourceLoading = R.layout.layout_loading_full_page),
            UniversalAdapterViewType.LoadingFooter<CodeListItem>(loaderFooter = R.layout.item_loading_more),
            UniversalAdapterViewType.NoData<CodeListItem>(),
            UniversalAdapterViewType.Error<CodeListItem>()
        ).build()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        viewModel.getNetworkErrorMapLiveData().observe(viewLifecycleOwner, Observer<String> { error ->
            if(error != "") {
                Toast.makeText(requireContext(), error, Toast.LENGTH_LONG).show()
            }
        })

        viewModel.getCodeLiveData().observe(viewLifecycleOwner, Observer<Resource<ArrayList<CodeMapItem>>> { list ->
            if (list.data != null) {
                list.data?.forEach { item ->
                    if (!markerList.containsKey(item.id)) {
                        markerList[item.id] = true
                        setMarker(item.location, item.title, item.id)
                    }
                }
            }
        })

        binding = FragmentHomeBinding.inflate(
            inflater,
            container,
            false
        )

        binding.lifecycleOwner = this
        setHasOptionsMenu(true)

        locationManager = requireContext().applicationContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager?
        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navigationController = Navigation.findNavController(view)

        homeView = view

        mapStateManager =
            MapStateManager(
                requireContext()
            )

        // set fab
        val fab: FloatingActionButton? = view.findViewById(R.id.fab)
        fab?.setOnClickListener {
            goToMyLocation()
        }
        val fab2: FloatingActionButton? = view.findViewById(R.id.fab2)
        fab2?.setOnClickListener {
            if (checkAndGetCameraPermission()) {
                initScan()
            }
        }

        mapView = view.findViewById(R.id.map) as MapView
        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync(this)

        searchListView = view.findViewById(R.id.search_background) as ConstraintLayout
        searchListView.visibility = Button.GONE


//        listView = view.findViewById(R.id.code_search)
//        val itemList = getListItems(5)
//        listView.adapter = SearchItemAdapter(itemList, this)
//        listView.layoutManager = LinearLayoutManager(this.context)
//        listView.setHasFixedSize(true)


        listView = view.findViewById(R.id.code_search)
        listView.layoutManager = LinearLayoutManager(this.context)
        listView.adapter = mAdapter

        viewModel.getCodeSearchLiveData().observe(viewLifecycleOwner, Observer {
            when(it.status){
                Status.LOADING -> {
                }
                Status.SUCCESS -> {
                    mAdapter.updateData(Resource.success(it.data))
                }
                else -> {}
            }
            mAdapter.notifyDataSetChanged()
        })
        viewModel.getNetworkErrorSearchLiveData().observe(
            viewLifecycleOwner,
            Observer<String> { error ->
                if (error != "") {
                    Toast.makeText(requireContext(), error, Toast.LENGTH_LONG).show()
                }
            })
    }

    override fun onDestroyView() {
        super.onDestroyView()

        if (gpsAlert !== null && gpsAlert!!.isShowing) gpsAlert!!.dismiss()
    }

    override fun onCreateOptionsMenu(menu: Menu, menuInflater: MenuInflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)

        searchButton = menu.findItem(R.id.action_search)
        val searchManager: SearchManager =
            requireActivity().getSystemService(Context.SEARCH_SERVICE) as SearchManager

        searchView = searchButton.actionView as SearchView
        if (searchView != null) {
            searchView!!.setSearchableInfo(searchManager.getSearchableInfo(requireActivity().componentName))
            queryTextListener = object : SearchView.OnQueryTextListener {
                override fun onQueryTextChange(newText: String): Boolean {
                    Log.i("onQueryTextChange", newText)
                    return true
                }

                override fun onQueryTextSubmit(query: String): Boolean {
                    Log.i("onQueryTextSubmit", query)
//                    searchListView.visibility = Button.GONE
//                    searchButton.collapseActionView()

                    if (currentSearchTerm != query) {
                        viewModel.emptySearchList()
                        val lat = googleMap.projection.visibleRegion.latLngBounds.center.latitude.toFloat()
                        val long = googleMap.projection.visibleRegion.latLngBounds.center.longitude.toFloat()
                        viewModel.getSearchList(query, 10, lat, long)
                    }

                    return true
                }
            }
            searchView!!.setOnQueryTextListener(queryTextListener)
        }

        menu.findItem(R.id.action_search).setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                //menu.findItem(R.id.action_filter).isVisible = false
                searchListView.visibility = Button.VISIBLE
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                //menu.findItem(R.id.action_filter).isVisible = true
                viewModel.emptySearchList()
                listView.adapter?.notifyDataSetChanged()
                currentSearchTerm = ""
                searchListView.visibility = Button.GONE
                return true
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_search -> {
                return false
            }
//            R.id.action_filter -> {
//                Snackbar.make(homeView, "action_filter", Snackbar.LENGTH_LONG)
//                    .setAction("Action", null).show()
//
//                return false
//            }
        }
        searchView?.setOnQueryTextListener(queryTextListener);
        return false
    }

    override fun onMapReady(map: GoogleMap) {
        markerList.clear()

        googleMap = map
        map.setOnCameraIdleListener {
            val region: VisibleRegion = googleMap.projection.visibleRegion
            val northeast: LatLng = region.latLngBounds.northeast
            val southwest: LatLng = region.latLngBounds.southwest
            val center: LatLng = region.latLngBounds.center

            val height: Double = northeast.latitude - southwest.latitude
            val width: Double = northeast.longitude - southwest.longitude

            if (height < 10)
                viewModel.getCodeMapList(center.longitude, center.latitude, width, height)
        }

        googleMap.setOnInfoWindowClickListener(this)

        if (checkAndGetLocationPermission())
            googleMap.isMyLocationEnabled = true

        googleMap.uiSettings.isMyLocationButtonEnabled = false
        googleMap.uiSettings.isMapToolbarEnabled = false

        val position: CameraPosition? = mapStateManager.savedCameraPosition
        if (position != null) {
            val update: CameraUpdate = CameraUpdateFactory.newCameraPosition(position)
            googleMap.moveCamera(update)

            googleMap.mapType = mapStateManager.savedMapType
        }

        mapView.onResume()

        mapView.getMapAsync { map ->
            val region: VisibleRegion = map.projection.visibleRegion
            val northeast: LatLng = region.latLngBounds.northeast
            val southwest: LatLng = region.latLngBounds.southwest
            val center: LatLng = region.latLngBounds.center

            val height: Double = (northeast.latitude - southwest.latitude) * 1.2
            val width: Double = (northeast.longitude - southwest.longitude) * 1.2

            if (height < 10)
                viewModel.getCodeMapList(center.longitude, center.latitude, width, height)
        }
    }

    override fun onInfoWindowClick(p0: Marker) {
        val action = HomeFragmentDirections.actionNavHomeToNavCodeDetails(p0.snippet)
        navigationController.navigate(action)
    }

    override fun onPause() {
        super.onPause()

        mapStateManager.saveMapState(googleMap)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            LOCATION_PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty()) {
                    var i = 0
                    while (i < grantResults.size) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            return
                        }
                        i++
                    }
                    @SuppressLint("MissingPermission")  // Has permissions
                    googleMap.isMyLocationEnabled = true
                }
            }
        }
        when (requestCode) {
            CAMERA_PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty()) {
                    var i = 0
                    while (i < grantResults.size) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            return
                        }
                        i++
                    }
                    // Has permissions
                    initScan()
                }
            }
        }
    }

    private fun checkAndGetLocationPermission() : Boolean {
        val permissions = arrayOf(
            FINE_LOCATION,
            COURSE_LOCATION
        )

        if (ContextCompat.checkSelfPermission(
                requireContext().applicationContext,
                FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
            && ContextCompat.checkSelfPermission(
                requireContext().applicationContext,
                COURSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        } else {
            requestPermissions(
                permissions,
                LOCATION_PERMISSION_REQUEST_CODE
            )
            return false
        }
    }

    private fun checkAndGetCameraPermission() : Boolean {
        val permissions = arrayOf(
            CAMERA
        )

        if (ContextCompat.checkSelfPermission(
                requireContext().applicationContext,
                CAMERA
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        } else {
            requestPermissions(
                permissions,
                CAMERA_PERMISSION_REQUEST_CODE
            )
            return false
        }
    }

    private fun bitmapDescriptorFromVector(context: Context, vectorResId:Int): BitmapDescriptor {
        val vectorDrawable : Drawable? = ContextCompat.getDrawable(context, vectorResId)
        vectorDrawable!!.setBounds(0, 0, vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight)
        val bitmap = Bitmap.createBitmap(vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas =  Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    private fun setMarker(coords: LatLng, title: String, snippet: String) {
        val markerIcon = bitmapDescriptorFromVector(requireContext(), R.drawable.ic_pointer)

        googleMap.addMarker(
            MarkerOptions()
                .position(coords)
                .title(title)
                .snippet(snippet)
                .icon(markerIcon)
        )
    }

    private fun goToMyLocation() {
        if (!checkAndGetLocationPermission()) return

        if (!locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER))
            buildAlertMessageNoGps()

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())

        fusedLocationClient.lastLocation
            .addOnSuccessListener { location ->
                // Got last known location. In some rare situations, this can be null.
                if (location != null) {
                    val position: CameraPosition? = CameraPosition.fromLatLngZoom(
                        LatLng(location.latitude, location.longitude),
                        16.0f
                    )
                    val update: CameraUpdate = CameraUpdateFactory.newCameraPosition(position)

                    googleMap.moveCamera(update)
                }
            }
    }

    private fun buildAlertMessageNoGps() {
        val builder = AlertDialog.Builder(requireContext())
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
            .setCancelable(false)
            .setPositiveButton("Yes"
            ) { _, _ -> startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)) }
            .setNegativeButton("No"
            ) { dialog, _ -> dialog.cancel() }
        gpsAlert = builder.create()
        gpsAlert!!.show()
    }

    private fun initScan() {
        val integrator: IntentIntegrator = IntentIntegrator.forSupportFragment(this)
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE)
        integrator.setPrompt("")
        integrator.setCameraId(0)
        integrator.setBeepEnabled(false)
        integrator.setBarcodeImageEnabled(false)
        integrator.initiateScan()
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        val result =
            IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            if (result.contents == null) {
                Toast.makeText(requireContext(), "Cancelled", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(requireContext(), "Scanned: " + result.contents, Toast.LENGTH_LONG).show()
                val action = HomeFragmentDirections.actionNavHomeToNavCodeRate(result.contents)
                navigationController.navigate(action)
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }
}