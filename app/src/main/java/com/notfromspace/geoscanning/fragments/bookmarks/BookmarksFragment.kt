package com.notfromspace.geoscanning.fragments.bookmarks

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.notfromspace.geoscanning.R

class BookmarksFragment : Fragment() {

    private lateinit var viewModel: BookmarksViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this).get(BookmarksViewModel::class.java)
        val view: View = inflater.inflate(R.layout.fragment_bookmarks, container, false)
        return view
    }
}