package com.notfromspace.geoscanning.fragments.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.notfromspace.geoscanning.models.CodeListItem
import com.notfromspace.geoscanning.models.CodeMapItem
import com.notfromspace.geoscanning.repositories.CodeMapRepository
import com.notfromspace.geoscanning.repositories.CodeSearchRepository
import com.yogeshpaliyal.universal_adapter.utils.Resource
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class HomeViewModel : ViewModel() {
    private var codeMapRepository: CodeMapRepository = CodeMapRepository()
    private var codeMapListLiveData: LiveData<Resource<ArrayList<CodeMapItem>>> = codeMapRepository.getCodesMapLiveData()
    private var networkErrorMapLiveData: LiveData<String> = codeMapRepository.getNetworkErrorLiveData()

    private var codeSearchRepository: CodeSearchRepository = CodeSearchRepository()
    private var codesSearchLiveData = codeSearchRepository.getCodesListLiveData()
    private var networkErrorSearchLiveData: LiveData<String> = codeSearchRepository.getNetworkErrorLiveData()
    var fetchJob: Job? = null


    fun getCodeMapList(long: Double, lat: Double, width: Double, height: Double) {
        codeMapRepository.getMapList(long, lat, width, height)
    }

    fun getCodeLiveData(): LiveData<Resource<ArrayList<CodeMapItem>>>{
        return codeMapListLiveData
    }

    fun getNetworkErrorMapLiveData(): LiveData<String> {
        return networkErrorMapLiveData
    }

    fun getSearchList(term: String, numberOfItems: Int, long: Float, lat: Float) {
        fetchJob = viewModelScope.launch {
            codeSearchRepository.getSearchList(term, numberOfItems, long, lat)
        }
    }

    fun emptySearchList() {
        codeSearchRepository.clearListLiveData()
    }

    fun getCodeSearchLiveData(): LiveData<Resource<ArrayList<CodeListItem>>> {
        return codesSearchLiveData
    }

    fun getNetworkErrorSearchLiveData(): LiveData<String> {
        return networkErrorSearchLiveData
    }
}