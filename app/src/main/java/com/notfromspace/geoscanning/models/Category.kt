package com.notfromspace.geoscanning.models

data class Category(
    val id: Int,
    val name: String
)

val Categories: List<Category> = listOf(
    Category(1, "Default"),
    Category(2, "Default2"),
    Category(3, "Default3"),
    Category(4, "Default4"),
    Category(5, "Default5"),
    Category(6, "Default6")
)