package com.notfromspace.geoscanning.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue
import java.time.LocalDateTime

@Parcelize
data class CodeDetails(
    val id: String,
    val dateHidden: LocalDateTime,
    val title: String,
    val category: @RawValue Category,
    val dif: Double,
    val funny: Double,
    val ownerId: Int,
    val ownerName: String,
    val long: Double,
    val lat: Double,
    val desc: String,
    val hint: String,
    var isBookmarked: Boolean
) : Parcelable
