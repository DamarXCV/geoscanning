package com.notfromspace.geoscanning.models

data class UserSession(
    val userId: Int,
    val token: String
) {
    override fun toString(): String {
        return "UserSession(userId=$userId, token='$token')"
    }
}