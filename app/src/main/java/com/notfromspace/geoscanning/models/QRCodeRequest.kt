package com.notfromspace.geoscanning.models

import com.google.gson.annotations.SerializedName

data class QRCodeRequest(
    @SerializedName("data") val data: String
)