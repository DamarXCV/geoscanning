package com.notfromspace.geoscanning.models

import com.google.gson.annotations.SerializedName
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

data class CodeRatingRequest(
    @SerializedName("id") val id: Int,
    @SerializedName("owner_id") val ownerId: Int,
    @SerializedName("owner_name") val ownerName: String,
    @SerializedName("date_found") val dateFound: String,
    @SerializedName("dif") val dif: Float,
    @SerializedName("fun") val funFactor: Float,
    @SerializedName("comment") val comment: String? = ""
) {
    fun codeRatingRequestToCodeRating(): CodeRating {
        val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
//    formatter =
//        formatter.withLocale(putAppropriateLocaleHere)
        val date = LocalDateTime.parse(this.dateFound, formatter)

        var comment = ""
        if (this.comment != null) {
            comment = this.comment
        }

        return CodeRating(
            this.id,
            this.ownerId,
            this.ownerName,
            date,
            this.dif,
            this.funFactor,
            comment
        )
    }
}