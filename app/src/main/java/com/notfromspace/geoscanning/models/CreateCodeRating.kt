package com.notfromspace.geoscanning.models

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.notfromspace.geoscanning.BR

data class CreateCodeRating(
    var codeId: String?,
    var dif: Float = 5f,
    var `fun`: Float = 5f,
    var comment: String = ""
) : BaseObservable() {
    var difToObserve: Float
        @Bindable get() = dif
        set(value) {
            dif = value
            notifyPropertyChanged(BR.difToObserve)
        }

    var funFactorToObserve: Float
        @Bindable get() = `fun`
        set(value) {
            `fun` = value
            notifyPropertyChanged(BR.funFactorToObserve)
        }

    var commentToObserve: String
        @Bindable get() = comment
        set(value) {
            comment = value
            notifyPropertyChanged(BR.commentToObserve)
        }

    override fun toString(): String {
        return "CreateCodeRating(codeId=$codeId, dif=$dif, `fun`=$`fun`, comment=$comment)"
    }
}