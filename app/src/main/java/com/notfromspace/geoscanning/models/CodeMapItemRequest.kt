package com.notfromspace.geoscanning.models

import com.google.android.gms.maps.model.LatLng
import com.google.gson.annotations.SerializedName

data class CodeMapItemRequest(
    @SerializedName("id") val id: String,
    @SerializedName("title") val title: String,
    @SerializedName("cat_id") val category: Int,
    @SerializedName("long") val long: Double,
    @SerializedName("lat") val lat: Double
) {
    fun codeMapItemRequestToCodeMapItem(): CodeMapItem {
        return CodeMapItem(
            this.id,
            this.title,
            Categories[this.category],
            LatLng(this.lat, this.long)
        )
    }
}