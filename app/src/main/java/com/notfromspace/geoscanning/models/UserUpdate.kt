package com.notfromspace.geoscanning.models

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.notfromspace.geoscanning.BR

data class UserUpdate(
    var firstname: String = "",
    var lastname: String = "",
    var alias: String = "",
    var hometown: String = "",
    var country: String = "",
    var email: String = "",
    var description: String = "",
    var profileImage: String? = ""
) : BaseObservable() {
    var firstnameToObserve: String
        @Bindable get() = firstname
        set(value) {
            firstname = value
            notifyPropertyChanged(BR.firstnameToObserve)
        }

    var lastnameToObserve: String
        @Bindable get() = lastname
        set(value) {
            lastname = value
            notifyPropertyChanged(BR.lastnameToObserve)
        }

    var aliasToObserve: String
        @Bindable get() = alias
        set(value) {
            alias = value
            notifyPropertyChanged(BR.aliasToObserve)
        }

    var hometownToObserve: String
        @Bindable get() = hometown
        set(value) {
            hometown = value
            notifyPropertyChanged(BR.hometownToObserve)
        }

    var countryToObserve: String
        @Bindable get() = country
        set(value) {
            country = value
            notifyPropertyChanged(BR.countryToObserve)
        }

    var emailToObserve: String
        @Bindable get() = email
        set(value) {
            email = value
            notifyPropertyChanged(BR.emailToObserve)
        }

    var descriptionToObserve: String
        @Bindable get() = description
        set(value) {
            description = value
            notifyPropertyChanged(BR.descriptionToObserve)
        }
}