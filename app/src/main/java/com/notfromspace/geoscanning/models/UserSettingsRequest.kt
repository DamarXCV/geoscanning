package com.notfromspace.geoscanning.models

import com.google.gson.annotations.SerializedName
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

data class UserSettingsRequest(
    @SerializedName("id") val id: Int,
    @SerializedName("alias") val alias: String,
    @SerializedName("firstname") val firstname: String,
    @SerializedName("lastname") val lastname: String,
    @SerializedName("email") val email: String,
    @SerializedName("joined_at") val joinedAt: String,
    @SerializedName("country") val country: String,
    @SerializedName("hometown") val hometown: String,
    @SerializedName("description") val description: String,
    @SerializedName("no_hidden_codes") val nOHiddenCodes: Int,
    @SerializedName("no_found_codes") val nOFoundCodes: Int,
    @SerializedName("profile_image") val profileImage: String?
) {
    fun userSettingsRequestToUserSettings(): UserSettings {

        val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
        //    formatter =
        //    formatter.withLocale(putAppropriateLocaleHere)
        val date = LocalDateTime.parse(this.joinedAt, formatter)

        return UserSettings(
            this.id,
            this.alias,
            this.firstname,
            this.lastname,
            this.email,
            date,
            this.country,
            this.hometown,
            this.description,
            this.nOHiddenCodes,
            this.nOFoundCodes,
            this.profileImage
        )
    }
}