package com.notfromspace.geoscanning.models

import com.google.gson.annotations.SerializedName
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

data class CodeDetailsRequest(
    @SerializedName("id") val id: String,
    @SerializedName("date_hidden") val dateHidden: String,
    @SerializedName("title") val title: String,
    @SerializedName("category") val category: Int,
    @SerializedName("dif") val dif: Double,
    @SerializedName("fun") val funny: Double,
    @SerializedName("owner_id") val ownerId: Int,
    @SerializedName("owner_name") val ownerName: String,
    @SerializedName("long") val long: Double,
    @SerializedName("lat") val lat: Double,
    @SerializedName("desc") val desc: String,
    @SerializedName("hint") val hint: String,
    @SerializedName("is_bookmarked") val isBookmarked: Boolean
) {
    fun CodeDetailsRequestToCodeDetails(): CodeDetails {
        val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
//    formatter =
//        formatter.withLocale(putAppropriateLocaleHere)
        val date = LocalDateTime.parse(this.dateHidden, formatter)

        return CodeDetails(
            this.id,
            date,
            this.title,
            Categories[this.category],
            this.dif,
            this.funny,
            this.ownerId,
            this.ownerName,
            this.long,
            this.lat,
            this.desc,
            this.hint,
            this.isBookmarked
        )
    }
}
