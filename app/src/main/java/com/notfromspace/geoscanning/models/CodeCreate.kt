package com.notfromspace.geoscanning.models

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.google.android.gms.maps.model.LatLng
import com.notfromspace.geoscanning.BR

data class CodeCreate(
    var title: String,
    val category: Category = Categories[0],
    var location: LatLng,
    var desc: String,
    var hint: String
) : BaseObservable() {
    var titleToObserve: String
        @Bindable get() = title
        set(value) {
            title = value
            notifyPropertyChanged(BR.titleToObserve)
        }

    var descToObserve: String
        @Bindable get() = desc
        set(value) {
            desc = value
            notifyPropertyChanged(BR.descToObserve)
        }

    var hintToObserve: String
        @Bindable get() = hint
        set(value) {
            hint = value
            notifyPropertyChanged(BR.hintToObserve)
        }
}
