package com.notfromspace.geoscanning.models

import java.time.LocalDateTime

data class UserSettings(
    val id: Int,
    val alias: String,
    val firstname: String,
    val lastname: String,
    val email: String,
    val joinedAt: LocalDateTime,
    val country: String,
    val hometown: String,
    val description: String,
    val nOHiddenCodes: Int,
    val nOFoundCodes: Int,
    val profileImage: String?
) {
    fun getJoinedAt(): String {
        return "${this.joinedAt.dayOfMonth}.${this.joinedAt.monthValue}.${this.joinedAt.year}"
    }

    fun getNOHiddenCodes(): String {
        return this.nOHiddenCodes.toString()
    }

    fun getNOFoundCodes(): String {
        return this.nOFoundCodes.toString()
    }
}