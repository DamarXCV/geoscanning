package com.notfromspace.geoscanning.models

import java.time.LocalDateTime

data class ScanCode(
    val id: Int,
    val codeId: String,
    val ownerId: Int,
    val ownerName: String,
    val dateFound: LocalDateTime,
    val dif: Float?,
    val funFactor: Float?,
    val comment: String? = ""
)