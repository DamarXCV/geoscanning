package com.notfromspace.geoscanning.models

import com.google.gson.annotations.SerializedName

data class UserSessionRequest(
    @SerializedName("user_id") val userId: Int,
    @SerializedName("token") val token: String
) {
    fun userSessionRequestToUserSession(): UserSession {
        return UserSession(
            this.userId,
            this.token
        )
    }
}