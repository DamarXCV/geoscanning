package com.notfromspace.geoscanning.models

import com.google.gson.annotations.SerializedName
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

data class ScanCodeRequest(
    @SerializedName("id") val id: Int,
    @SerializedName("code_id") val codeId: String,
    @SerializedName("owner_id") val ownerId: Int,
    @SerializedName("owner_name") val ownerName: String,
    @SerializedName("date_found") val dateFound: String,
    @SerializedName("dif") val dif: Float?,
    @SerializedName("fun") val funFactor: Float?,
    @SerializedName("comment") val comment: String? = ""
) {
    fun codeRatingRequestToCodeRating(): ScanCode {
        val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
//    formatter =
//        formatter.withLocale(putAppropriateLocaleHere)
        val date = LocalDateTime.parse(this.dateFound, formatter)

        var comment = ""
        if (this.comment != null) {
            comment = this.comment
        }

        return ScanCode(
            this.id,
            this.codeId,
            this.ownerId,
            this.ownerName,
            date,
            this.dif,
            this.funFactor,
            comment
        )
    }
}