package com.notfromspace.geoscanning.models

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.notfromspace.geoscanning.BR

data class UserLogin(
    var email: String = "",
    var password: String = ""
) : BaseObservable() {
    var emailToObserve: String
        @Bindable get() = email
        set(value) {
            email = value
            notifyPropertyChanged(BR.emailToObserve)
        }

    var passwordToObserve: String
        @Bindable get() = password
        set(value) {
            password = value
            notifyPropertyChanged(BR.passwordToObserve)
        }

}