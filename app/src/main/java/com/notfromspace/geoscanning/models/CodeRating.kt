package com.notfromspace.geoscanning.models

import java.time.LocalDateTime

data class CodeRating(
    val id: Int,
    val ownerId: Int,
    val ownerName: String,
    val dateFound: LocalDateTime,
    val dif: Float,
    val funFactor: Float,
    val comment: String
) {
    fun getDateFound(): String {
        return "${this.dateFound.dayOfMonth}.${this.dateFound.monthValue}.${this.dateFound.year}"
    }
}