package com.notfromspace.geoscanning.models

import com.google.android.gms.maps.model.LatLng

data class CodeMapItem(
    val id: String,
    val title: String,
    val category: Category,
    val location: LatLng
)