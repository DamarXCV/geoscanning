package com.notfromspace.geoscanning.models

import com.google.android.gms.maps.model.LatLng
import com.google.gson.annotations.SerializedName

data class CodeListItemRequest(
    @SerializedName("id") val id: String,
    @SerializedName("title") val title: String,
    @SerializedName("category") val category: Int,
    @SerializedName("dif") val dif: Double,
    @SerializedName("fun") val funFactor: Double,
    @SerializedName("long") val long: Double,
    @SerializedName("lat") val lat: Double
) {
    fun codeListItemRequestToCodeListItem(): CodeListItem {
        return CodeListItem(
            this.id,
            this.title,
            Categories[this.category],
            this.dif.toFloat(),
            this.funFactor.toFloat(),
            LatLng(this.lat, this.long)
        )
    }
}
