package com.notfromspace.geoscanning.models

import com.google.android.gms.maps.model.LatLng

data class CodeListItem(
    val id: String,
    val title: String,
    val category: Category,
    val dif: Float,
    val funFactor: Float,
    val location: LatLng
) {
    fun getLat(): String {
        return this.location.latitude.toString()
    }

    fun getLng(): String {
        return this.location.longitude.toString()
    }
}