package com.notfromspace.geoscanning

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.notfromspace.geoscanning.extensions.PersistentData
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit


class StartActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        PersistentData(applicationContext)

        Log.v("GetUserSession", PersistentData.GetUserSession().toString())
        Executors.newSingleThreadScheduledExecutor().schedule({
            if (PersistentData.GetUserSession() != null) {
                startActivity(Intent(this, MainActivity::class.java))
            } else {
                startActivity(Intent(this, AuthActivity::class.java))
            }
        }, 1, TimeUnit.SECONDS)
    }
}