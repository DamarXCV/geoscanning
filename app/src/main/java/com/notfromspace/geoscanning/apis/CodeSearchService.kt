package com.notfromspace.geoscanning.apis

import com.notfromspace.geoscanning.models.CodeListItemRequest
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface CodeSearchService {
    @GET("/codes/codes_search.php")
    fun GetSearchList(
        @Query("term") query: String,
        @Query("number_of_items") query2: Int,
        @Query("long") query3: Float,
        @Query("lat") query4: Float
    ): Call<List<CodeListItemRequest>>

}
