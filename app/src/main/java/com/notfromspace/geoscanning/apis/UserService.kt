package com.notfromspace.geoscanning.apis

import com.notfromspace.geoscanning.models.UserRequest
import com.notfromspace.geoscanning.models.UserSettingsRequest
import retrofit2.Call
import retrofit2.http.*


interface UserService {
    @GET("/users/users.php?")
    fun GetUser(
        @Query("user_id") query: Int
    ): Call<UserRequest>

    @GET("/users/users_settings.php")
    fun GetUserSettings(
    ): Call<UserSettingsRequest>

    @FormUrlEncoded
    @PATCH("/users/users.php")
    fun UpdateUser(
        @Field("firstname") query: String,
        @Field("lastname") query2: String,
        @Field("alias") query3: String,
        @Field("hometown") query4: String,
        @Field("country") query5: String,
        @Field("email") query6: String,
        @Field("desc") query7: String,
        @Field("profile_image") query8: String?
    ): Call<UserRequest>
}