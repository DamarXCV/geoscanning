package com.notfromspace.geoscanning.apis

import com.notfromspace.geoscanning.models.CodeRatingRequest
import com.notfromspace.geoscanning.models.ScanCodeRequest
import retrofit2.Call
import retrofit2.http.*


interface CodeRatingService {
    @GET("/code_ratings/code_ratings.php")
    fun GetRatingList(
        @Query("code_id") query: String,
        @Query("before_date") query2: String,
        @Query("start_position") query3: Int,
        @Query("number_of_items") query4: Int
    ): Call<List<CodeRatingRequest>>

    @FormUrlEncoded
    @POST("/code_ratings/codes_scan.php")
    fun LogCode(
        @Field("key") query: String
    ): Call<ScanCodeRequest>

    @FormUrlEncoded
    @PATCH("/code_ratings/code_ratings.php")
    fun RateCode(
        @Field("code_id") query: String,
        @Field("dif") query2: Float,
        @Field("fun") query3: Float,
        @Field("comment") query4: String
    ): Call<CodeRatingRequest>
}