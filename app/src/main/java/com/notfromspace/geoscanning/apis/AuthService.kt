package com.notfromspace.geoscanning.apis

import com.notfromspace.geoscanning.models.UserSessionRequest
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST


interface AuthService {

    @FormUrlEncoded
    @POST("/users/users.php")
    fun CreateUser(
        @Field("firstname") query: String,
        @Field("lastname") query2: String,
        @Field("alias") query3: String,
        @Field("hometown") query4: String,
        @Field("country") query5: String,
        @Field("email") query6: String,
        @Field("desc") query7: String?,
        @Field("profile_image") query8: String?,
        @Field("password") query9: String
    ): Call<UserSessionRequest>

    @FormUrlEncoded
    @POST("/users/login.php")
    fun Login(
        @Field("email") query: String,
        @Field("password") query1: String
    ): Call<UserSessionRequest>

}