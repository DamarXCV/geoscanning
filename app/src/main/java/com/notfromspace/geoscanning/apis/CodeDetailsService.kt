package com.notfromspace.geoscanning.apis

import com.google.gson.JsonObject
import com.notfromspace.geoscanning.models.CodeDetailsRequest
import com.notfromspace.geoscanning.models.QRCodeRequest
import retrofit2.Call
import retrofit2.http.*


interface CodeDetailsService {
    @GET("/codes/codes.php")
    fun GetCode(
        @Query("code_id") query: String
    ): Call<CodeDetailsRequest>

    @FormUrlEncoded
    @POST("/codes/codes_bookmarks.php")
    fun AddBookmark(
        @Field("code_id") query: String
    ): Call<JsonObject>

    @DELETE("/codes/codes_bookmarks.php")
    fun RemoveBookmark(
        @Query("code_id") query: String
    ): Call<JsonObject>

    @GET("/codes/codes_qr.php")
    fun GetQRCode(
        @Query("code_id") query: String
    ): Call<QRCodeRequest>
}