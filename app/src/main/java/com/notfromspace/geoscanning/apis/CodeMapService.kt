package com.notfromspace.geoscanning.apis

import com.notfromspace.geoscanning.models.CodeMapItemRequest
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface CodeMapService {
    @GET("/codes/codes_map.php")
    fun GetMapList(
        @Query("long") query: Double,
        @Query("lat") query2: Double,
        @Query("width") query3: Double,
        @Query("height") query4: Double
    ): Call<List<CodeMapItemRequest>>
}
