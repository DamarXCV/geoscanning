package com.notfromspace.geoscanning.apis

import com.notfromspace.geoscanning.models.CodeDetailsRequest
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface CodeCreateService {
    @FormUrlEncoded
    @POST("/codes/codes.php")
    fun CreateCode(
        @Field("title") query: String,
        @Field("category") query2: Int,
        @Field("long") query3: Float,
        @Field("lat") query4: Float,
        @Field("desc") query5: String,
        @Field("hint") query6: String
    ): Call<CodeDetailsRequest>
}