package com.notfromspace.geoscanning.apis

import com.notfromspace.geoscanning.models.CodeListItemRequest
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface CodeListService {
    @GET("/codes/codes_created.php")
    fun GetCreatedList(
        @Query("user_id") query: Int,
        @Query("before_date") query2: String,
        @Query("start_position") query3: Int,
        @Query("number_of_items") query4: Int
    ): Call<List<CodeListItemRequest>>

    @GET("/codes/codes_found.php")
    fun GetFoundList(
        @Query("user_id") query: Int,
        @Query("before_date") query2: String,
        @Query("start_position") query3: Int,
        @Query("number_of_items") query4: Int
    ): Call<List<CodeListItemRequest>>

    @GET("/codes/codes_bookmarks.php")
    fun GetBookmarkList(
        @Query("user_id") query: Int,
        @Query("before_date") query2: String,
        @Query("start_position") query3: Int,
        @Query("number_of_items") query4: Int
    ): Call<List<CodeListItemRequest>>

}
